﻿using Firebase.Database;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AdminLogin : MonoBehaviour
{
    public static AdminLogin instance;
    const int templateElementSize = 200;
    [SerializeField]
    GameObject loginBtn;
    [SerializeField]
    GameObject logoutnBtn;
    [SerializeField]
    RectTransform reviewContainer;
    [SerializeField]
    GameObject reviewPrefab;
    int itemCount = 0;
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        instance = this;
        AuthenticationManager.FirebaseTrigger += LoadMenu;
        //if (AuthenticationManager.instance.auth == null)
        //{
        //    loginBtn.SetActive(false);
        //    logoutnBtn.SetActive(true);
        //}
        //else
        //{
        //    loginBtn.SetActive(true);
        //    logoutnBtn.SetActive(false);
        //}
    }
    void LoadMenu(bool isSigned)
    {
        if (!isSigned)
        {
            loginBtn.SetActive(true);
            logoutnBtn.SetActive(false);
        }
        else
        {
            loginBtn.SetActive(false);
            logoutnBtn.SetActive(true);
            LoadReview();
        }
    }
    public void Login()
    {
        AuthenticationManager.instance.FirebaseLogin("admin@admin.com", "qH{f-bSgY%5.5`uN");
    }
    public void Logout()
    {
        AuthenticationManager.instance.auth.SignOut();
        AdminLoading.Instance.LoadNewScene(AdminScene.MenuAdmin);
    }
    async void LoadReview()
    {
        List<Templates> templates = await FirebaseData.GetTemplatesForReview();
        foreach (Templates s in templates)
        {
            ReviewElement element = Instantiate(reviewPrefab, reviewContainer).GetComponent<ReviewElement>();
            if (s.challengeId == "")
                element.CreateElement(false, s.templateName ,s.templateId);
            else
                element.CreateElement(true, s.templateName, s.templateId);
            itemCount += templateElementSize;
            reviewContainer.sizeDelta = new Vector2(0, itemCount);
        }
    }
    public async void DisplayTemplate(string templateId)
    {
        Templates t = await FirebaseData.GetTemplate(templateId);
        AdminLoading.Instance.displayTemp = t;
        AdminLoading.Instance.LoadNewScene(AdminScene.GameAdmin);
    }
    private void OnDestroy()
    {
        AuthenticationManager.FirebaseTrigger += LoadMenu;
    }
}
