﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using TMPro;
using UnityEngine.SceneManagement;

public enum AdminScene
{
    MenuAdmin,
    GameAdmin
}
public class AdminLoading : MonoBehaviour
{
    private static AdminLoading instance;
    [SerializeField]
    GameObject downloadPopup;
    [SerializeField]
    TextMeshProUGUI progressTxt;
    public bool isClearCache;
    [SerializeField]
    AssetBundle[] mBundle;
    [SerializeField]
    string[] mUri;
    public Templates displayTemp;
    public static AdminLoading Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<AdminLoading>();
                if (instance == null)
                {
                    instance = new GameObject("AdminLoading", typeof(AdminLoading)).GetComponent<AdminLoading>();
                }
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
    }
    void Awake()
    {
        if (instance == null)
        {
            instance = this; // In first scene, make us the singleton.
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
            Destroy(gameObject);
        mBundle = new AssetBundle[mUri.Length];
        Caching.compressionEnabled = false;
        StartCoroutine(GetBundle());
    }
    IEnumerator GetBundle()
    {
        for (int i = 0; i < mUri.Length; i++)
        {
            using (var www = new UnityWebRequest(mUri[i], UnityWebRequest.kHttpVerbGET))
            {
                www.downloadHandler = new DownloadHandlerAssetBundle(mUri[i], 0, 0);
                www.SendWebRequest();
                while (!www.isDone)
                {
                    progressTxt.text = (Mathf.Round(www.downloadProgress * 100)).ToString() + "%";
                    yield return null;
                }
                if (!www.isNetworkError)
                {
                    mBundle[i] = DownloadHandlerAssetBundle.GetContent(www);
                }
            }
        }
        downloadPopup.SetActive(false);
    }
    public Sprite GetSprite(string name)
    {
        Sprite img = null;
        for (int i = 0; i < mBundle.Length; i++)
        {
            img = mBundle[i].LoadAsset<Sprite>(name);
            if (img != null)
            {
                break;
            }
        }
        return img;
    }
    public void LoadNewScene(AdminScene sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName.ToString());
    }
}
