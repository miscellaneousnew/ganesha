﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ReviewElement : MonoBehaviour
{
    [SerializeField]
    GameObject challenge;
    [SerializeField]
    TextMeshProUGUI templateNameTxt;
    [SerializeField]
    Button visitButton;

    public void CreateElement(bool isChallenge, string templateName, string templateId)
    {
        challenge.SetActive(isChallenge);
        templateNameTxt.text = templateName;
        name = templateId;
        visitButton.onClick.AddListener(delegate { AdminLogin.instance.DisplayTemplate(transform.name); });
    }
    private void OnDestroy()
    {
        visitButton.onClick.RemoveAllListeners();
    }
}
