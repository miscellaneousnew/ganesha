﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TMPro;
public class AdminGame : MonoBehaviour
{

    //public TMP_InputField ratingTxt;
    public Slider ratingVal;
    public TextMeshProUGUI ratingTxt;
    public TMP_InputField gemsTxt;
    public TMP_InputField coinsTxt;
    public RectTransform area;
    public GameObject item;
    Templates template;
    AdminLoading aLoading = AdminLoading.Instance;

    void Start()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        template = aLoading.displayTemp;
        LoadExistingTemplate();
    }
    public void onSliderChange()
    {
        ratingTxt.text = ratingVal.value.ToString("F2");
    }
    public async void BestBtn()
    {
        if(ratingVal.value != 0)
        {
            int gems = 0;
            if(gemsTxt.text != "")
            {
                gems = int.Parse(gemsTxt.text);
            }
            int coins = 0;
            if (coinsTxt.text != "")
            {
                coins = int.Parse(coinsTxt.text);
            }
            await FirebaseData.ReviewTemplate( template, int.Parse(ratingTxt.text), ConstStr.BESTTEMPLATES, gems, coins,true);
            aLoading.LoadNewScene(AdminScene.MenuAdmin);
        }
    }
    public async void SpecialBtn()
    {
        if (ratingVal.value != 0)
        {
            int gems = 0;
            if (gemsTxt.text != "")
            {
                gems = int.Parse(gemsTxt.text);
            }
            int coins = 0;
            if (coinsTxt.text != "")
            {
                coins = int.Parse(coinsTxt.text);
            }
            await FirebaseData.ReviewTemplate(template, int.Parse(ratingTxt.text), ConstStr.SPECIALTEMPLATES,gems,coins, true);
            aLoading.LoadNewScene(AdminScene.MenuAdmin);
        }
    }
    public async void NewBtn()
    {
        if (ratingVal.value != 0)
        {
            int gems = 0;
            if (gemsTxt.text != "")
            {
                gems = int.Parse(gemsTxt.text);
            }
            int coins = 0;
            if (coinsTxt.text != "")
            {
                coins = int.Parse(coinsTxt.text);
            }
            await FirebaseData.ReviewTemplate(template, int.Parse(ratingTxt.text), ConstStr.NEWTEMPLATES, gems, coins,true);
            aLoading.LoadNewScene(AdminScene.MenuAdmin);
        }
    }
    public async void ApproveBtn()
    {
        if (ratingVal.value != 0)
        {
            int gems = 0;
            if (gemsTxt.text != "")
            {
                gems = int.Parse(gemsTxt.text);
            }
            int coins = 0;
            if (coinsTxt.text != "")
            {
                coins = int.Parse(coinsTxt.text);
            }
            await FirebaseData.ReviewTemplate(template, int.Parse(ratingTxt.text), "", gems, coins,false);
            aLoading.LoadNewScene(AdminScene.MenuAdmin);
        }
    }
    void LoadExistingTemplate()
    {
        string categoryId;
        string itemId;
        string[] assetId;
        for (int i = 0; i < template.templateItems.Length; i++)
        {
            assetId = template.templateItems[i].assetId.Split(',');
            categoryId = assetId[0];
            itemId = assetId[1];
            Item itm = Instantiate(item, area).GetComponent<Item>();
            Sprite sp = aLoading.GetSprite(itemId);
            itm.CreateItem(sp, categoryId.ToString() + "," + itemId.ToString(), false);
            itm.transform.localPosition = StringToVector(template.templateItems[i].position);
            itm.transform.rotation = Quaternion.Euler(new Vector3(0, 0, float.Parse(template.templateItems[i].rotation)));
            RectTransform childTransform = itm.GetComponent<RectTransform>();
            childTransform.sizeDelta = new Vector2(template.templateItems[i].width, template.templateItems[i].height);
        }
    }
    Vector3 StringToVector(string pos)
    {
        string[] temp = pos.Split(',');
        return new Vector3(float.Parse(temp[0]), float.Parse(temp[1]), float.Parse(temp[2]));
    }
}
