using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public enum SingleSelectEnum
{
    Best,
    Special,
    New,
    All,
    Completed,
    Play,
    Home,
    Friends,
    GetCash
}
public class SingleSelectType : MonoBehaviour
{
    public SingleSelectEnum type;
    public Sprite selectedImg;
    public Sprite notSelectedImg;
    Image image;
    private void Start() {
        image = GetComponent<Image>();
    }
    public void OnClick(bool isEnable){
        if(isEnable){
            image.sprite = selectedImg;
        }else{
            image.sprite = notSelectedImg;
        }
    }
}
