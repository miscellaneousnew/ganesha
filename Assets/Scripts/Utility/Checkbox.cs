﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Checkbox : MonoBehaviour
{
    public Sprite checkedImg;
    public Sprite uncheckedImg;
    [SerializeField]
    Image image;
    public static event Action onChange;
    public bool isChecked = false;
    public void OnClick(){
        if(isChecked){
            image.sprite = uncheckedImg;
        }else{
            image.sprite = checkedImg;
        }
        isChecked = !isChecked;
        onChange?.Invoke();
    }
}
