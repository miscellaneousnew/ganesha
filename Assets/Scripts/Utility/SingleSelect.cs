﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingleSelect : MonoBehaviour
{
    [SerializeField]
    SingleSelectType[] singleSelectTyp;
    [SerializeField]
    SingleSelectEnum defautType;
    private void Start() {
        for (int i = 0; i < singleSelectTyp.Length; i++)
        {
            if (singleSelectTyp[i].type == defautType)
            {
                singleSelectTyp[i].OnClick(true);
                break;
            }
        }
    }
    public void OnSingleSelect(SingleSelectType single){
        for (int i = 0; i < singleSelectTyp.Length; i++)
        {
            if(singleSelectTyp[i] == single){
                singleSelectTyp[i].OnClick(true);
            }else{
                singleSelectTyp[i].OnClick(false);
            }
        }
    }
}
