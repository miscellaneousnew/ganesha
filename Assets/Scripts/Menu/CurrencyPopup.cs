﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;

public class CurrencyPopup : MonoBehaviour
{
    [SerializeField]
    GameObject popup;
    
    public void OpenPopup()
    {
        if(popup!=null)
            popup.SetActive(true);
    }
    public void Shop()
    {
        ClosePopup();
        MenuManager.instance.StoreButton();
    }
    public void ClosePopup()
    {
        if (popup != null)
            popup.SetActive(false);
    }
}