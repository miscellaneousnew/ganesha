﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public class FriendsPanel : MonoBehaviour
{
    const int templateElementSize = 200;
    [SerializeField]
    Transform friendListContainer;
    [SerializeField]
    GameObject friendElementPrefab;
    int friendsCount;
    GameInstanceManager gameInstanceManager;
    // Start is called before the first frame update
    void Awake()
    {
        gameInstanceManager = GameInstanceManager.Instance;
    }

    private void OnEnable()
    {
        LoadFriendList();
    }
    async void LoadFriendList()
    {
        Task<List<Friend>> task = FirebaseData.GetFriendsList(gameInstanceManager.user.UserId);
        List<Friend> friends = await task;
        foreach(Friend f in friends)
        {
            FriendsElement fElement = Instantiate(friendElementPrefab, friendListContainer).GetComponent<FriendsElement>();
            fElement.CreateFriendElement(f.playerId,f.playerName,f.templateId);
            friendsCount += templateElementSize;
            friendListContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(0, friendsCount);
        }
    }
    private void OnDisable()
    {
        foreach (Transform child in friendListContainer.transform)
        {
            Destroy(child.gameObject);
        }
        friendsCount = 0;
    }
}
