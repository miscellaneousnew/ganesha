﻿using System.Collections.Generic;
using UnityEngine;

public class StorePanel : MonoBehaviour
{
    [SerializeField]
    RectTransform storeListContainer;
    [SerializeField]
    GameObject storeElementPrefab;
    GameInstanceManager gameInstanceManager;
    // Start is called before the first frame update
    void Awake()
    {
        gameInstanceManager = GameInstanceManager.Instance;
    }

    private void OnEnable()
    {
        if (storeListContainer != null)
        {
            LoadShopItems();
        }
    }
    async void LoadShopItems()
    {
        List<Store> storeData = await FirebaseData.GetStoreList();
        foreach (Store s in storeData)
        {
            StoreElement storeElement = Instantiate(storeElementPrefab, storeListContainer).GetComponent<StoreElement>();
            if (s.isGem)
                storeElement.CreateStoreElement(GameCurrency.Gem, s.quantity, s.percent, s.amount, s.id);
            else
                storeElement.CreateStoreElement(GameCurrency.Gold, s.quantity, s.percent, s.amount, s.id);
        }
    }
    private void OnDisable()
    {
        if(storeListContainer != null)
        {
            foreach (Transform child in storeListContainer.transform)
            {
                Destroy(child.gameObject);
            }
        }
    }
}
