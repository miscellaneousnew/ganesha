﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class TemplatePanel : MonoBehaviour
{
    const int templateElementSize = 240;
    [SerializeField]
    Button createBtn;
    [SerializeField]
    TMP_InputField templateNameInput;
    [SerializeField]
    Transform templateContainer;
    [SerializeField]
    GameObject templateElementPrefab;
    [SerializeField]
    CurrencyPopup currencyPopup;
    int templateSize;
    GameInstanceManager gameInstanceManager;
    void Awake()
    {
        gameInstanceManager = GameInstanceManager.Instance;
    }
    private void OnEnable()
    {
        LoadMyTemplates();
    }
    private void OnDisable()
    {
        foreach (Transform child in templateContainer.transform)
        {
            Destroy(child.gameObject);
        }
        templateSize = 0;
    }
    async void LoadMyTemplates()
    {
        List<MyTemplates> myTemplates = await FirebaseData.GetUserTemplates( gameInstanceManager.user.UserId);
        foreach (MyTemplates t in myTemplates)
        {
            TemplateElement itm = Instantiate(templateElementPrefab, templateContainer.transform).GetComponent<TemplateElement>();
            itm.SetupTemplate(t.templateId, t.templateName, t.isLive);
            templateSize += templateElementSize;
            templateContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(0, templateSize);
        }
    }
    public void CreateTemplate()
    {
        if(templateNameInput.text != "")
        {
            GameInstanceManager.insufficientCurreny += OpenCurrencyPopup;
            gameInstanceManager.CreateNewTemplate(templateNameInput.text);
        }
    }
    void OpenCurrencyPopup()
    {
        GameInstanceManager.insufficientCurreny -= OpenCurrencyPopup;
        currencyPopup.OpenPopup();
    }
    public void OnTextChange()
    {
        if(templateNameInput.text == "")
        {
            createBtn.interactable = false;
        }
        else
        {
            createBtn.interactable = true;
        }
    }
}
