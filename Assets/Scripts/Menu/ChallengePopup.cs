﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ChallengePopup : MonoBehaviour
{
    [SerializeField]
    GameObject popup;
    [SerializeField]
    RawImage img;
    [SerializeField]
    TextMeshProUGUI title1Txt;
    [SerializeField]
    TextMeshProUGUI title2Txt;
    [SerializeField]
    TextMeshProUGUI gemReward;
    [SerializeField]
    TextMeshProUGUI coinReward;
    [SerializeField]
    TextMeshProUGUI descriptionTxt;
    [SerializeField]
    Button designBtn;
    string id;
    public async void OpenChallengePopup(string userId,Challenges challenges)
    {
        img.texture = await GameInstanceManager.Instance.GetImage(challenges.imageName);
        title1Txt.text = challenges.title1;
        title2Txt.text = challenges.title2;
        descriptionTxt.text = challenges.description;
        gemReward.text = challenges.gemAmount.ToString();
        coinReward.text = challenges.coinAmount.ToString();
        popup.SetActive(true);
        id = await FirebaseData.GetMyExistingChallenge(userId,challenges.challengeId);
        designBtn.onClick.AddListener(delegate { GameInstanceManager.Instance.LoadTemplate(id, true); });
    }
    public void ClosePopup()
    {
        popup.SetActive(false);
        designBtn.onClick.RemoveAllListeners();
    }
}
