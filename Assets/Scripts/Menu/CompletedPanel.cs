﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class CompletedPanel : MonoBehaviour
{
    const int templateElementSize = 300;
    [SerializeField]
    Transform completedContainer;
    [SerializeField]
    GameObject completedElementPrefab;

    int templateSize;
    GameInstanceManager gameInstanceManager;
    void Awake()
    {
        gameInstanceManager = GameInstanceManager.Instance;
    }
    private void OnEnable()
    {
        LoadCompletedTemplates();
    }
    private void OnDisable()
    {
        foreach (Transform child in completedContainer.transform)
        {
            Destroy(child.gameObject);
        }
        templateSize = 0;
    }
    async void LoadCompletedTemplates()
    {
        List<CompletedChallengeTemplates> myTemplates = await FirebaseData.GetUserCompletedTemplates( gameInstanceManager.user.UserId);
        foreach (CompletedChallengeTemplates t in myTemplates)
        {
            CompleteChallengeElement itm = Instantiate(completedElementPrefab, completedContainer.transform).GetComponent<CompleteChallengeElement>();
            itm.CreateTemplate(t.templateName,t.rating,t.gemAmount, t.coinAmount, t.inReview);
            templateSize += templateElementSize;
            completedContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(0, templateSize);
        }
    }
}
