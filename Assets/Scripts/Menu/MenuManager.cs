﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using ImageAndVideoPicker;
public class MenuManager : MonoBehaviour
{
    public static MenuManager instance;
    const int artElementSize = 250;
    const string artByCommunity = "ART BY COMMUNITY";
    const string artByFriends = "ART BY COMMUNITY";
    [SerializeField]
    RawImage profilePic;
    [SerializeField]
    TextMeshProUGUI username;
    [SerializeField]
    TextMeshProUGUI gemTxt;
    [SerializeField]
    TextMeshProUGUI goldTxt;
    [SerializeField]
    GameObject profilePopup;
    [Header("Profile")]
    [SerializeField]
    RawImage editProfilePic;
    [SerializeField]
    TextMeshProUGUI editUserName;
    [SerializeField]
    TMP_InputField userNameInput;
    [Header("Filters")]
    [SerializeField]
    TextMeshProUGUI sectionHeader;
    [SerializeField]
    Transform artTemplateContainer;
    [SerializeField]
    Transform bestTemplates;
    [SerializeField]
    Transform specialTemplates;
    [SerializeField]
    Transform newTemplates;
    [SerializeField]
    Transform allTemplates;
    [SerializeField]
    GameObject artElementPrefab;
    [Header("Challenges")]
    [SerializeField]
    Transform challengesContainer;
    [SerializeField]
    GameObject challengesElementPrefab;
    [SerializeField]
    ChallengePopup challengePopup;
    [Header("Notification")]
    [SerializeField]
    TextMeshProUGUI notificationCount;
    [SerializeField]
    GameObject notify;
    [SerializeField]
    NotificationPopup notificationPopup;
    [Header("Panels")]
    [SerializeField]
    GameObject completedPanel;
    [SerializeField]
    GameObject createTemplatePanel;
    [SerializeField]
    GameObject homePanel;
    [SerializeField]
    GameObject friendPanel;
    [SerializeField]
    GameObject storePanel;
    int itemCount;
    Texture2D texture_512;
    GameInstanceManager gameInstanceManager;
    Dictionary<string, string> newNotifications;
    bool isNotified;
    void OnEnable()
    {
        PickerEventListener.onImageLoad += OnImageLoad;

        #if UNITY_ANDROID
                AndroidPicker.CheckPermissions();
        #endif
    }

    void OnDisable()
    {
        PickerEventListener.onImageLoad -= OnImageLoad;
    }
    private void Awake()
    {
        instance = this;
        gameInstanceManager = GameInstanceManager.Instance;
        GameInstanceManager.purchaseMade += UpdateUI;
    }
    void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        FetchPlayerData();
        BestTemplates();
        LoadChallenges();
        UpdateNotificationCount();
        gameInstanceManager.GetProfileImage(profilePic, gameInstanceManager.user.UserId);
    }
    async void FetchPlayerData()
    {
        PlayerData playerData = await FirebaseData.FetchPlayerData(gameInstanceManager.user.UserId);
        if(playerData != null)
        {
            gameInstanceManager.playerData = playerData;
            gameInstanceManager.playerData.playerId = gameInstanceManager.user.UserId;
            gemTxt.text = playerData.currentGems.ToString();
            goldTxt.text = playerData.currentGold.ToString();
        }
        username.text = await gameInstanceManager.GetUsername();
    }
    void UpdateUI()
    {
        gemTxt.text = gameInstanceManager.playerData.currentGems.ToString();
        goldTxt.text = gameInstanceManager.playerData.currentGold.ToString();
    }
    public async void OnProfileClick()
    {
        gameInstanceManager.GetProfileImage(editProfilePic, gameInstanceManager.user.UserId);
        editUserName.text = await gameInstanceManager.GetUsername();
        profilePopup.SetActive(true);
    }
    public void LogoutBtn()
    {
        AuthenticationManager.instance.LogOut();
    }
    public void EditProfilePic()
    {
        AndroidPicker.BrowseImage(true);
        //NativeGallery.Permission permission = NativeGallery.GetImageFromGallery((path) =>
        //{
        //    Debug.Log("Image path: " + path);
        //    if (path != null)
        //    {
        //        // Create Texture from selected image
        //        texture_512 = NativeGallery.LoadImageAtPath(path, 512);
        //        if (texture_512 == null)
        //        {
        //            Debug.Log("Couldn't load texture from " + path);
        //            return;
        //        }
        //        editProfilePic.texture = texture_512;
        //    }
        //}, "Select a PNG image", "image/png");

        //Debug.Log("Permission result: " + permission);
    }
    void OnImageLoad(string imgPath, Texture2D tex, ImageAndVideoPicker.ImageOrientation imgOrientation)
    {
        //Debug.Log("Image Location : " + imgPath);
        //log += "\n OnImageLoad : Image Path : " + imgPath;
        editProfilePic.texture = tex;
        texture_512 = tex;
    }
    public async void EditProfileSave()
    {
        if(texture_512 != null)
        {
            await FirebaseData.UploadProfilePic(texture_512, gameInstanceManager.storage, gameInstanceManager.user.UserId);
            profilePic.texture = texture_512;
        }
        if (userNameInput.text != "")
        {
            username.text = userNameInput.text;
            AuthenticationManager.instance.UpdateUserData(userNameInput.text);
        }
        profilePopup.SetActive(false);
    }
    public void CloseEditProfile()
    {
        profilePopup.SetActive(false);
    }
    public void CompletedButton()
    {
        completedPanel.SetActive(true);
        createTemplatePanel.SetActive(false);
        homePanel.SetActive(false);
        //friendPanel.SetActive(false);
        storePanel.SetActive(false);
    }
    public void PlayButton()
    {
        completedPanel.SetActive(false);
        createTemplatePanel.SetActive(true);
        homePanel.SetActive(false);
        //friendPanel.SetActive(false);
        storePanel.SetActive(false);
    }
    public void HomeButton()
    {
        FetchPlayerData();
        completedPanel.SetActive(false);
        createTemplatePanel.SetActive(false);
        homePanel.SetActive(true);
        //friendPanel.SetActive(false);
        storePanel.SetActive(false);
    }
    //public void FriendsButton()
    //{
    //    completedPanel.SetActive(false);
    //    createTemplatePanel.SetActive(false);
    //    homePanel.SetActive(false);
    //    friendPanel.SetActive(true);
    //    storePanel.SetActive(false);
    //}

    public void WinFreeButton()
    {
        AdManager.RewardTrigger += OnReward;
        AdManager.RewardedVideoAdClosed += OnRewardClosed;
        AdManager.Instance.ShowRewardedVideo();
    }
    void OnRewardClosed()
    {
        AdManager.RewardTrigger -= OnReward;
        AdManager.RewardedVideoAdClosed -= OnRewardClosed;
    }
    void OnReward()
    {
        AdManager.RewardTrigger -= OnReward;
        FetchRewardAndUpdate();
    }
    async void FetchRewardAndUpdate()
    {
        WinFree winFree = await FirebaseData.GetReward();
        int coin = Random.Range(winFree.coinMin, winFree.coinMax);
        int gem = Random.Range(winFree.gemMin, winFree.gemMax);
        gameInstanceManager.playerData.currentGold += coin;
        gameInstanceManager.playerData.currentGems += gem;
        UpdateUI();
        await FirebaseData.UpdatePlayerData(gameInstanceManager.playerData);
    }
    public void StoreButton()
    {
        completedPanel.SetActive(false);
        createTemplatePanel.SetActive(false);
        homePanel.SetActive(false);
        friendPanel.SetActive(false);
        storePanel.SetActive(true);
    }
    public void BestTemplates()
    {
        sectionHeader.text = artByCommunity;
        bestTemplates.gameObject.SetActive(true);
        specialTemplates.gameObject.SetActive(false);
        newTemplates.gameObject.SetActive(false);
        allTemplates.gameObject.SetActive(false);
        if (bestTemplates.childCount == 0)
        //    artTemplateContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(bestTemplates.childCount * artElementSize, 0);
        //else
            LoadArtTemplate(bestTemplates, ConstStr.BESTTEMPLATES);
    }
    public void SpecialTemplates()
    {
        sectionHeader.text = artByCommunity;
        bestTemplates.gameObject.SetActive(false);
        specialTemplates.gameObject.SetActive(true);
        newTemplates.gameObject.SetActive(false);
        allTemplates.gameObject.SetActive(false);
        if (specialTemplates.childCount == 0)
        //    artTemplateContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(specialTemplates.childCount * artElementSize, 0);
        //else
            LoadArtTemplate(specialTemplates, ConstStr.SPECIALTEMPLATES);
    }
    public void NewTemplates()
    {
        sectionHeader.text = artByCommunity;
        bestTemplates.gameObject.SetActive(false);
        specialTemplates.gameObject.SetActive(false);
        newTemplates.gameObject.SetActive(true);
        allTemplates.gameObject.SetActive(false);
        if (newTemplates.childCount == 0)
        //    artTemplateContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(newTemplates.childCount * artElementSize, 0);
        //else
            LoadArtTemplate(newTemplates, ConstStr.NEWTEMPLATES);
    }
    public void FreindsTemplates()
    {
        sectionHeader.text = artByFriends;
        bestTemplates.gameObject.SetActive(false);
        specialTemplates.gameObject.SetActive(false);
        newTemplates.gameObject.SetActive(false);
        allTemplates.gameObject.SetActive(true);
        if (allTemplates.childCount == 0)
        //    artTemplateContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(allTemplates.childCount * artElementSize, 0);
        //else
            LoadFriendsArtTemplate(allTemplates);
    }
    async void LoadArtTemplate(Transform templateTransform ,string templateType)
    {
        if (templateTransform == null)
            return;
        List<LiveTemplate> templates = await FirebaseData.GetTemplateList(templateType);
        int i = 0;
        foreach(LiveTemplate s in templates)
        {
            ArtElement artElement = Instantiate(artElementPrefab, templateTransform).GetComponent<ArtElement>();
            artElement.CreateArtElement(i,s.playerId,s.playerName,s.templateId);
            i++;
            itemCount += artElementSize;
            artTemplateContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(itemCount, 0);
        }
    }
    async void LoadFriendsArtTemplate(Transform templateTransform)
    {
        if (templateTransform == null)
            return;
        List<Friend> friends = await FirebaseData.GetFriendsList(gameInstanceManager.user.UserId);
        int i = 0;
        foreach(Friend s in friends)
        {
            ArtElement storeElement = Instantiate(artElementPrefab, templateTransform).GetComponent<ArtElement>();
            storeElement.CreateArtElement(i,s.playerId,s.playerName,s.templateId);
            i++;
            itemCount += artElementSize;
            artTemplateContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(itemCount, 0);
        }
    }
    async void LoadChallenges()
    {
        if (challengesContainer == null)
            return;
        List<Challenges> challenges = await FirebaseData.GetChallenges(gameInstanceManager.user.UserId);
        int i = 0;
        foreach (Challenges c in challenges)
        {
            ChallengeElement challengeElement = Instantiate(challengesElementPrefab, challengesContainer).GetComponent<ChallengeElement>();
            challengeElement.CreateChallengeElement(c);
            i++;
        }
    }
    public void LoadChallengePopup(Challenges challenges)
    {
        gameInstanceManager.challenge = challenges;
        challengePopup.OpenChallengePopup(gameInstanceManager.playerData.playerId,challenges);
    }
    public void OpenNotificationPopup()
    {
        if (!isNotified)
        {
            isNotified = true;
            notificationPopup.OpenNotificationPopup(newNotifications);
            FirebaseData.UpdateNotification(newNotifications, gameInstanceManager.user.UserId);
        }
        else
        {
            notificationPopup.OpenNotificationPopup();
        }
    }
    async void UpdateNotificationCount()
    {
        newNotifications = await FirebaseData.FetchNotification(gameInstanceManager.user.UserId);
        if(newNotifications.Count > 0)
        {
            notificationCount.text = newNotifications.Count.ToString();
            notify.SetActive(true);
        }
        else
        {
            notify.SetActive(false);
        }
    }
}
