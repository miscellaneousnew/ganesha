﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections.Generic;

public class NotificationPopup : MonoBehaviour
{
    const int templateElementSize = 240;
    [SerializeField]
    GameObject popup;
    [SerializeField]
    GameObject notificationElementPrefab;
    [SerializeField]
    RectTransform notificationContainer;
    int templateSize;
    [SerializeField]
    GameObject notification;
    [SerializeField]
    GameObject noNotification;
    public void OpenNotificationPopup(Dictionary<string,string> newMsg)
    {
        if(newMsg.Count == 0)
        {
            noNotification.SetActive(true);
        }
        else
        {
            noNotification.SetActive(false);
            foreach (KeyValuePair<string, string> msg in newMsg)
            {
                NotificationElement itm = Instantiate(notificationElementPrefab, notificationContainer.transform).GetComponent<NotificationElement>();
                itm.SetupMsg(msg.Value);
                templateSize += templateElementSize;
                notificationContainer.sizeDelta = new Vector2(0, templateSize);
            }
        }
        popup.SetActive(true);
    }
    public void OpenNotificationPopup()
    {
        popup.SetActive(true);
    }
    public void ClosePopup()
    {
        popup.SetActive(false);
        notification.SetActive(false);
    }
}