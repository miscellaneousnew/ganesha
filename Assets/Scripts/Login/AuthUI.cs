﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class AuthUI : MonoBehaviour
{
    //[SerializeField]
    //TMP_InputField mobileInput;
    [SerializeField]
    TMP_InputField emailInput;
    [SerializeField]
    TMP_InputField passwordInput;
    [SerializeField]
    GameObject registerLoginPage;
    [SerializeField]
    GameObject registerPage;
    [SerializeField]
    GameObject loginPage;
    
    private void Start()
    {
        Screen.orientation = ScreenOrientation.Portrait;
        Back();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Back();
        }
    }
    public void OpenLoginScreen()
    {
        registerLoginPage.SetActive(false);
        emailInput.gameObject.SetActive(true);
        passwordInput.gameObject.SetActive(true);
        loginPage.SetActive(true);
    }
    public void OpenRegisterScreen()
    {
        registerLoginPage.SetActive(false);
        emailInput.gameObject.SetActive(true);
        passwordInput.gameObject.SetActive(true);
        registerPage.SetActive(true);
    }
    public void Back()
    {
        registerLoginPage.SetActive(true);
        emailInput.gameObject.SetActive(false);
        passwordInput.gameObject.SetActive(false);
        registerPage.SetActive(false);
        loginPage.SetActive(false);
    }
    public void Register()
    {
        if (emailInput.text != "" && passwordInput.text != null)
            AuthenticationManager.instance.FirebaserRegister(emailInput.text, passwordInput.text);
        else
            Debug.Log("Enter Information Correctly");
    }
    
    public void Login()
    {
        if (emailInput.text != "" && passwordInput.text != null)
            AuthenticationManager.instance.FirebaseLogin(emailInput.text, passwordInput.text);
        else
            Debug.Log("Enter Information Correctly");
    }
}
