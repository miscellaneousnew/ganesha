﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Database;
using Firebase.Unity.Editor;
using Firebase.Auth;
using Firebase.Storage;

public class AuthenticationManager : MonoBehaviour
{
    public static event Action<bool> FirebaseTrigger;
    public static event Action<bool,string> FirebasePhoneTrigger;
    public static AuthenticationManager instance;
    public FirebaseAuth auth;
    public DatabaseReference db;
    public FirebaseUser user;
    bool signedIn;
    bool linkEmail = false;
    [SerializeField]
    Texture2D profilePic;
    public Firebase.FirebaseApp app;
    string verificationId = "";
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            //This will be the only instance of the type FireBaseHandler in the Game
        }
        else if (instance != this)
        {
            Destroy(this.gameObject);
            //Any other instance will be destroyed
        }
        DontDestroyOnLoad(this.gameObject);
    }

    void Start()
    {
        
        CheckInitialization();
    }
    void CheckInitialization()
    {
        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                app = Firebase.FirebaseApp.DefaultInstance;
                app.SetEditorDatabaseUrl(ConstStr.FIREBASEDB);
                auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
                auth.StateChanged += AuthStateChanged;
                //AuthStateChanged(this, null);
                //isFirebaseReady = true;
                db = FirebaseDatabase.DefaultInstance.RootReference;
                Debug.Log("Firebase initialized successfully");
            }
            else
            {
                // Firebase Unity SDK is not safe to use here.
                Debug.LogError(string.Format("Could not resolve all Firebase dependencies: {0}", dependencyStatus));
            }

        });
    }

    void AuthStateChanged(object sender, EventArgs e)
    {
        if (auth.CurrentUser != user)
        {
            signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                Debug.Log("Signed out " + user.UserId);
            }
            user = auth.CurrentUser;
            if (signedIn)
            {
                Debug.Log("Signed in " + user.UserId);
            }
        }
        FirebaseTrigger?.Invoke(signedIn);
    }
    #region Public Methods
    public void LogOut()
    {
        auth.SignOut();
        LoadingManager.Instance.LoadNewScene(GameScene.Loading);
    }

    public bool RecheckEmailVerfication()
    {
        auth.CurrentUser.ReloadAsync();
        return auth.CurrentUser.IsEmailVerified;
    }
    public void FirebaserRegister(string email, string password)
    {
        Register(email, password);
    }
    async void Register(string email, string password)
    {
        bool isUserCreated = false;
        await auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                return;
            }
            // Firebase user has been created.
            isUserCreated = true;
        });
        if (isUserCreated)
        {
            //LoadingManager.Instance.MobileVerificationPopup(true);
            UpdateUserData(email.Split('@')[0]);
        }        
    }
    public void PhoneVerification(string mobile)
    {
        PhoneAuthProvider provider = PhoneAuthProvider.GetInstance(auth);
        provider.VerifyPhoneNumber(mobile, 30, null,
            verificationCompleted: (credential) => {
                // Auto-sms-retrieval or instant validation has succeeded (Android only).
                // There is no need to input the verification code.
                // `credential` can be used instead of calling GetCredential().
            },
            verificationFailed: (error) => {
                Debug.Log(error);
                // The verification code was not sent.
                // `error` contains a human readable explanation of the problem.
            },
            codeSent: (id, token) => {
                Debug.Log(id);
                verificationId = id;
                //FirebasePhoneTrigger?.Invoke(true, id);
                // Verification code was successfully sent via SMS.
                // `id` contains the verification id that will need to passed in with
                // the code from the user when calling GetCredential().
                // `token` can be used if the user requests the code be sent again, to
                // tie the two requests together.
            }
            //,
            //codeAutoRetrievalTimeout: (id) => {
            //    // Called when the auto-sms-retrieval has timed out, based on the given
            //    // timeout parameter.
            //    // `id` contains the verification id of the request that timed out.
            //}
        );
        
    }
    public async void StartMobileVerification(string verificationCode)
    {
        bool isVerified = false;
        PhoneAuthProvider provider = PhoneAuthProvider.GetInstance(auth);
        Credential credential = provider.GetCredential(verificationId, verificationCode);
        await auth.CurrentUser.LinkWithCredentialAsync(credential).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("LinkWithCredentialAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("LinkWithCredentialAsync encountered an error: " + task.Exception);
                return;
            }
            if (task.IsCompleted)
            {
                isVerified = true;
            }
        });
        if (isVerified)
        {
            FirebaseStorage storage = FirebaseStorage.GetInstance(app, ConstStr.STORAGE);
            await FirebaseData.UploadProfilePic(profilePic, storage, user.UserId);
            FirebaseData.CreateNewUser(user.UserId);
            //LoadingManager.Instance.MobileVerificationPopup(false);
            LoadingManager.Instance.LoadNewScene(GameScene.Loading);
        }
        else
        {
            Debug.Log("Error");
        }
    }
    public async void AnonymousLogin(string name)
    {
        bool isUserCreated = false;
        await auth.SignInAnonymouslyAsync().ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInAnonymouslyAsync was canceled.");
                return;
            }
            if (task.IsFaulted)
            {
                Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
                return;
            }
            isUserCreated = true;
        });
        if (isUserCreated)
        {
            FirebaseData.CreateNewUser(user.UserId);
            UpdateUserData(name);
            FirebaseStorage storage = FirebaseStorage.GetInstance(app, ConstStr.STORAGE);
            await FirebaseData.UploadProfilePic(profilePic, storage, user.UserId);
        }
        LoadingManager.Instance.LoadNewScene(GameScene.Menu);
    }
    public void FirebaseLogin(string email, string password)
    {
        auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task =>
        {
            if (task.IsFaulted)
            {
                Debug.Log("Failed To Login");
            }
            else
            {
                Debug.Log("Login");
            }
        });
    }
    //public void AnonymousLogin(string username)
    //{
    //    if (createUserDetail != null)
    //        StopCoroutine(createUserDetail);
    //    createUser = false;
    //    createUserDetail = StartCoroutine(CreateUser(username));
    //    auth.SignInAnonymouslyAsync().ContinueWith(task => {
    //        if (task.IsCanceled)
    //        {
    //            Debug.LogError("SignInAnonymouslyAsync was canceled.");
    //            return;
    //        }
    //        if (task.IsFaulted)
    //        {
    //            Debug.LogError("SignInAnonymouslyAsync encountered an error: " + task.Exception);
    //            return;
    //        }
    //        else
    //        {
    //            createUser = true;
    //        }
    //    });
    //}
    #endregion



    #region Create New User 
    //IEnumerator CreateUser()
    //{
    //    yield return new WaitUntil(() => createUser == true);
    //    //bool isFetchingAppSetting = false;
    //    //AppSettings appSettings = new AppSettings();
    //    //FirebaseDatabase.DefaultInstance.GetReference(ConstStr.APPSETTINGS).GetValueAsync().ContinueWith(task =>
    //    //{
    //    //    if (task.IsFaulted)
    //    //    {
    //    //        Debug.Log("Faulted");
    //    //    }
    //    //    else if (task.IsCompleted)
    //    //    {
    //    //        string json = task.Result.GetRawJsonValue();
    //    //        if (json != null)
    //    //        {
    //    //            appSettings = JsonUtility.FromJson<AppSettings>(json);
    //    //            isFetchingAppSetting = true;
    //    //        }
    //    //    }
    //    //});
    //    //yield return new WaitUntil(() => isFetchingAppSetting == true);
    //    PlayerData pd = new PlayerData(user.UserId,100,1000);
    //    db.Child(ConstStr.PLAYERS + user.UserId).SetRawJsonValueAsync(JsonUtility.ToJson(pd));
    //    yield return new WaitForSeconds(.1f);
    //    AuthStateChanged(this, null);
    //}
    
    public async void UpdateUserData(string username)
    {
        await db.Child(ConstStr.PLAYERS + user.UserId).Child(ConstStr.PLAYERNAME).SetValueAsync(username);
        //FirebaseUser user = auth.CurrentUser;
        //if (user != null)
        //{
        //    UserProfile profile;
        //    if (photo == "")
        //    {
        //        profile = new UserProfile
        //        {
        //            DisplayName = username,
        //        };
        //    }
        //    else
        //    {
        //        profile = new UserProfile
        //        {
        //            DisplayName = username,
        //            PhotoUrl = new Uri(photo),
        //        };
        //    }
        //    await user.UpdateUserProfileAsync(profile).ContinueWith(task => {
        //        if (task.IsCanceled)
        //        {
        //            Debug.LogError("UpdateUserProfileAsync was canceled.");
        //            return;
        //        }
        //        if (task.IsFaulted)
        //        {
        //            Debug.LogError("UpdateUserProfileAsync encountered an error: " + task.Exception);
        //            return;
        //        }

        //        Debug.Log("User profile updated successfully.");
        //    });
        //    await db.Child(ConstStr.PLAYERS + user.UserId).Child(ConstStr.PLAYERNAME).SetValueAsync(username);
        //}
    }
    #endregion
   
    
    // void PictureCallBack(IGraphResult result)
    // {
    //     string path = Application.persistentDataPath + PROFILEPIC;
    //     byte[] bytes = result.Texture.EncodeToPNG();
    //     File.WriteAllBytes(path, bytes);
    //     playerPhoto = PERSISTENTDATAPATH + PROFILEPIC;
    //     isPhotoReady = true;
    //     //Texture2D image = result.Texture;
    //     //img.sprite = Sprite.Create(image, new Rect(0, 0, 100, 100), new Vector2(0.5f, 0.5f));
    // }
    // void NameCallBack(IGraphResult result)
    // {
    //     IDictionary<string, object> profile = result.ResultDictionary;
    //     playerName = profile["first_name"].ToString();
    //     isNameReady = true;
    // }

    void OnDestroy()
    {
        if (auth != null)
        {
            auth.StateChanged -= AuthStateChanged;
            auth = null;
        }
    }
}