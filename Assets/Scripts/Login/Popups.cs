﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;

public class Popups : MonoBehaviour, IPointerClickHandler
{
    const string TOC = "Terms of Service";
    const string PP = "Privacy Policy";
    [SerializeField]
    TextMeshProUGUI linksTxt;
    [SerializeField]
    GameObject createButtonArea;
    [SerializeField]
    GameObject detailPopup;
    [SerializeField]
    TextMeshProUGUI headerTxt;
    [SerializeField]
    TextMeshProUGUI descriptionTxt;
    [SerializeField]
    Button confirmBtn;

    public async void OpenLinkPopup(string type, string txt)
    {
        if (type == "PP")
        {
            headerTxt.text = PP;
            descriptionTxt.text = await FirebaseData.GetPrivacy();
        }
        else
        {
            headerTxt.text = TOC;
            descriptionTxt.text = await FirebaseData.GetConditions();
        }
        detailPopup.SetActive(true);
    }
    public void OpenCloseCreateBtnArea(bool isActive)
    {
        createButtonArea.SetActive(isActive);
    }
    public void CloseLinkPopup()
    {
        detailPopup.SetActive(false);
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        int linkIndex = TMP_TextUtilities.FindIntersectingLink(linksTxt, eventData.position, eventData.pressEventCamera);
        if (linkIndex != -1)
        {
            TMP_LinkInfo linkInfo = linksTxt.textInfo.linkInfo[linkIndex];
            OpenLinkPopup(linkInfo.GetLinkID(), linkInfo.GetLinkText());
        }
    }
}
