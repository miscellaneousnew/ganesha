﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LoadingScene : MonoBehaviour
{
    [SerializeField]
    GameObject[] clouds;
    [SerializeField]
    float[] cloudsSpeed;
    [SerializeField]
    Popups popups;
    [SerializeField]
    GameObject downloadPopup;
    [SerializeField]
    TextMeshProUGUI progressTxt;
    [SerializeField]
    Button createBtn;
    float position = 350;
    private void Start()
    {
        CloudAnimation();
        LoadingManager.progress += UpdateDownload;
        LoadingManager.DownloadComplete += OnDownloadComplete;
        AuthenticationManager.FirebaseTrigger += LoadMenu;
        if (LoadingManager.Instance.isAssetReady)
        {
            OnDownloadComplete();
        }
    }
    void CloudAnimation()
    {
        for (int i = 0; i < clouds.Length; i++)
        {
            LeanTween.moveLocalX(clouds[i], position, cloudsSpeed[i]).setLoopType(LeanTweenType.linear).setRepeat(-1);
        }
    }
    void OnDownloadComplete()
    {
        downloadPopup.SetActive(false);
        popups.OpenCloseCreateBtnArea(true);
    }
    public void CreateBtn()
    {
        if (LoadingManager.Instance.isActiveUser)
        {
            Debug.Log(AuthenticationManager.instance.auth.CurrentUser.UserId);
            //StartCoroutine(LoadMenu());
            LoadingManager.Instance.LoadNewScene(GameScene.Menu);
        }
        else
        {
            string username = ConstStr.GenerateName(6);
            AuthenticationManager.instance.AnonymousLogin(username);
            //LoadNewScene(GameScene.Login);
        }
    }
    void UpdateDownload(float percent)
    {
        progressTxt.text = (Mathf.Round(percent * 100)).ToString() + "%";
    }
    private void OnDestroy()
    {
        LoadingManager.progress -= UpdateDownload;
        LoadingManager.DownloadComplete -= OnDownloadComplete;
        AuthenticationManager.FirebaseTrigger -= LoadMenu;
    }
    void LoadMenu(bool isActiveuser)
    {
        createBtn.interactable = true;
    }
}
