using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class Item : MonoBehaviour, IPointerClickHandler, IDragHandler
{
    [SerializeField]
    Image img;
    [SerializeField]
    RectTransform imgTransform;
    Vector3 pos;
    bool isSelected;
    public bool isGem;
    public int amount;
    public void ShowControls(bool isEnabled){
        isSelected = isEnabled;
        SelectionControl.instance.Select(this, imgTransform, isEnabled);
    }
    public void OnDrag(PointerEventData eventData)
    {

        if (isSelected)
        {
            pos = new Vector2(eventData.position.x, eventData.position.y);
            transform.position = pos;
            SelectionControl.instance.transform.position = pos;
            //currentItem.transform.position = pos;
        }
    }
    public void CreateItem(GameCurrency gameCurrency, int amount,Sprite sprite, string Id,bool isControl){
        img.sprite = sprite;
        name = Id;
        if (gameCurrency == GameCurrency.Gem)
            isGem = true;
        else
            isGem = false;
        this.amount = amount;
        FixPerspective();
        ShowControls(isControl);
        //deleteBtn.onClick.AddListener(delegate { GameManager.instance.RemoveElement(this); });
    }
    public void OnPointerClick(PointerEventData eventData)
    {
        isSelected = true;
        ShowControls(true);
        //GameManager.instance.SelectItem(this);
    }
    public void CreateItem(string spritePath, string Id, bool isControl)
    {
        img.sprite = LoadingManager.Instance.GetSprite(spritePath);
        FixPerspective();
        name = Id;
        ShowControls(isControl);
    }
    public void CreateItem(Sprite sp, string Id, bool isControl)
    {
        img.sprite = sp;
        FixPerspective();
        name = Id;
        ShowControls(isControl);
    }
    void FixPerspective()
    {
        img.SetNativeSize();
        //if(imgTransform.rect.width > 400 || imgTransform.rect.height > 400)
        //{
        //    imgTransform.sizeDelta = new Vector3(imgTransform.rect.width/5, imgTransform.rect.height/ 5);
        //}
    }
    //public void OnPointerClick(PointerEventData eventData)
    //{
    //    GameManager.instance.SelectItem(this);
    //}
    //public void OnDrag(PointerEventData eventData)
    //{
    //    GameManager.instance.SelectItem(this);
    //    if(isSelected){
    //        transform.position = new Vector2(eventData.position.x, eventData.position.y);
    //    }
    //}
    private void OnDestroy()
    {
        //deleteBtn.onClick.RemoveAllListeners();
    }
}
