﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class UIManager : MonoBehaviour
{
    public static UIManager instance;
    public ScrollRect topBar;
    public ScrollRect bottomBar;
    public TextMeshProUGUI gemUI;
    public TextMeshProUGUI goldUI;

    void Awake() {
        instance = this;    
    }
    public void UpdateUI(int gemCount,int goldCount){
        gemUI.text = gemCount.ToString();
        goldUI.text = goldCount.ToString();
    }

    #region NAVIGATION IN SCROLLVIEWS

    public void TopBarLeftClicked()
    {
        topBar.horizontalNormalizedPosition = topBar.horizontalNormalizedPosition - 0.1f;
    }

    public void TopBarRightClicked()
    {
        topBar.horizontalNormalizedPosition = topBar.horizontalNormalizedPosition + 0.1f;
    }

    public void BottomBarRightClicked()
    {
        bottomBar.horizontalNormalizedPosition = bottomBar.horizontalNormalizedPosition + 0.1f;
    }

    public void BottomBarLeftClicked()
    {
        bottomBar.horizontalNormalizedPosition = bottomBar.horizontalNormalizedPosition - 0.1f;
    }
    #endregion
}
