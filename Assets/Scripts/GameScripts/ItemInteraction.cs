﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ItemInteraction : MonoBehaviour ,IDragHandler
{
    [SerializeField]
    RectTransform selectionTransform;
    RectTransform itemTransform;

    Vector3 endPos;
    Vector2 pos;
    public float mul = 9.5f;
    Vector2 startSize = new Vector2(0,0);
    public void ScaleImage(RectTransform imgTransform)
    {
        itemTransform = imgTransform;
        pos = itemTransform.sizeDelta;
    }
    public void OnDrag(PointerEventData eventData)
    {
        endPos = eventData.position;
        var dir = endPos - selectionTransform.position;
        var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        var rotation = Quaternion.AngleAxis(angle + 45 * (selectionTransform.rect.height/ selectionTransform.rect.width) , Vector3.forward);
        var size = dir.magnitude * pos * mul *(selectionTransform.rect.height / selectionTransform.rect.width) / Screen.width;
        selectionTransform.rotation = rotation;
        selectionTransform.sizeDelta = size + startSize;
        itemTransform.rotation = rotation;
        itemTransform.sizeDelta = size;
    }
}
