public class PlayerData
{
    public string playerId;
    public int currentGold;
    public int currentGems;
    public PlayerData(string playerId,int gem, int gold)
    {
        this.playerId = playerId;
        currentGold = gold;
        currentGems = gem;
    }
}
public class Friend
{
    public string playerId;
    public string playerName;
    public string templateId;
    public Friend(string id,string name,string tId)
    {
        playerId = id;
        playerName = name;
        templateId = tId;
    }
}
public class MyTemplates
{
    public string templateId;
    public string templateName;
    public bool isLive;
}
public class CompletedChallengeTemplates
{
    public bool inReview;
    public string templateName;
    public float rating;
    public int gemAmount;
    public int coinAmount;
}
public class MyFriends
{
    public string friendId;
}
public class Store
{
    public string id;
    public bool isGem;
    public int quantity;
    public int percent;
    public int amount;
    public Store(string itemId, bool isGem,int itemQuantity ,int itemPercent, int itemAmount)
    {
        id = itemId;
        this.isGem = isGem;
        quantity = itemQuantity;
        percent = itemPercent;
        amount = itemAmount;
    }
}
public class PlayersLiveData{
    public string playerId;
    public string liveTemplateId;
}
public class LiveTemplate{
    public string playerId;
    public string playerName;
    public string templateId;
    public LiveTemplate(string id, string name, string tId)
    {
        playerId = id;
        playerName = name;
        templateId = tId;
    }
}
public class Templates{
    public string playerId;
    public string templateId;
    public string templateName;
    public float rating;
    public bool inReview;
    public bool isCompleted;
    public string challengeId;
    public TemplateItems[] templateItems;
}
[System.Serializable]
public struct TemplateItems
{
    public string assetId;
    public string position;
    public string rotation;
    public float width;
    public float height;
    public bool isFlip;
}
[System.Serializable]
public class Challenges{
    public string challengeId;
    public int gemAmount;
    public int coinAmount;
    public string title1;
    public string title2;
    public string imageName;
    public string description;
    public Categories[] categories;
}
[System.Serializable]
public struct Categories
{
    public string categoryId;
    public string position;
}
public class TemplateForReview{
    public string templateId;
}

public class ContainerData
{
    public string categoryId;
    public ItemData[] itemData;
}
[System.Serializable]
public struct ItemData {
    public string itemId;
    public int amount;
    public bool isGem;
}
public class WinFree
{
    public int coinMin;
    public int coinMax;
    public int gemMin;
    public int gemMax;
}
public class ItemPurchased
{
    public bool isGem;
    public int amount;
}