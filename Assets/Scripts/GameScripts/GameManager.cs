﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;
using TMPro;

public class GameManager : MonoBehaviour
{
    //const string HideAll = "HIDE All";
    //const string ShowAll = "SHOW All";
    const int itemElementSize = 90;
    const int categoryElementSize = 100;
    public static GameManager instance;
    int gemsCount;
    int goldCount;
    int itemSize;
    int categorySize;
    public GameObject gameArea;
    public GameObject editUi;
    public GameObject displayUi;
    public GameObject displayGameArea;
    [SerializeField]
    GamePopup gamePopup;

    [Header("References")]
    public RectTransform categoryContainer;
    public RectTransform itemContainer;
    public RectTransform pointersContainer;

    [Header("Prefabs")]
    public GameObject categoryElement;
    public GameObject itemElement;
    public GameObject item;
    public GameObject pointerPrefab;
    
    Item selectedItem;
    GameInstanceManager gameInstanceManager;
    Templates template;
    ContainerData contrData;

    //[SerializeField]
    //TextMeshProUGUI toggleTxt;
    //bool isHide;
    [SerializeField]
    GameObject artTxt;
    [SerializeField]
    GameObject showBtn;
    [SerializeField]
    GameObject gameUiBG;
    [SerializeField]
    RectTransform gameAreaTransform;
    float left = 107.5905f;
    float right = 15.39026f;
    float top = 116.5747f;
    float bottom = 81.97463f;
    [SerializeField]
    GameObject shopPopup;
    [SerializeField]
    GameObject confirmPopup;
    float scaleSize = .77f;
    void Awake() {
        instance = this;
        gameInstanceManager = GameInstanceManager.Instance;
        GameInstanceManager.purchaseMade += UpdateUI;
    }
    void UpdateUI()
    {
        gemsCount = gameInstanceManager.playerData.currentGems;
        goldCount = gameInstanceManager.playerData.currentGold;
        UIManager.instance.UpdateUI(gemsCount,goldCount);
    }
    void Start()
    {
        if (gameInstanceManager.isTemplate)
        {
            template = gameInstanceManager.currentTemplate;
            if (gameInstanceManager.isDisplay)
            {
                DisplayUI();
            }
            else
            {
                EditUI();
            }
        }
        else
        {
            EditTemplate();
            template = gameInstanceManager.currentTemplate;
        }
        LoadCategoryPointer();
    }
    void LoadCategoryPointer()
    {
        if (gameInstanceManager.isChallenge)
        {
            foreach (Categories c in gameInstanceManager.challenge.categories)
            {
                Pointer p = Instantiate(pointerPrefab, pointersContainer.transform).GetComponent<Pointer>();
                p.transform.localPosition = StringToVector(c.position);
                p.SetupPointer(c.categoryId);
            }
        }
    }
    void DisplayUI()
    {
        editUi.SetActive(false);
        displayUi.SetActive(true);
        LoadExistingTemplate(displayGameArea.transform);
    }
    void EditTemplate()
    {
        gemsCount = gameInstanceManager.playerData.currentGems;
        goldCount = gameInstanceManager.playerData.currentGold;
        UIManager.instance.UpdateUI(gemsCount, goldCount);
        LoadAllContainer();
        editUi.SetActive(true);
        displayUi.SetActive(false);
    }
    void EditUI()
    {
        EditTemplate();
        LoadExistingTemplate(gameArea.transform);
    }
    public void BringToTop()
    {
        if (selectedItem)
            selectedItem.transform.SetAsLastSibling();
    }

    public void BringToBottom()
    {
        if (selectedItem)
            selectedItem.transform.SetAsFirstSibling();
    }
    //public void HideShow()
    //{
    //    if (isHide)
    //    {
    //        isHide = false;
    //        Show();
    //    }
    //    else
    //    {
    //        isHide = true;
    //        Hide();
    //    }
        
    //}
    public void Hide()
    {
        //gameAreaTransform.offsetMin = new Vector2(0, 0);
        //gameAreaTransform.offsetMax = new Vector2(0, 0);
        gameAreaTransform.localScale = Vector3.one;
        gameUiBG.SetActive(false);
        showBtn.SetActive(true);
        artTxt.SetActive(true);
        //if(selectedItem){
        //    selectedItem.ShowControls(false);
        //}
        //gameArea.SetActive(false);
    }

    public void Show()
    {
        //gameAreaTransform.offsetMin = new Vector2(left, bottom);
        //gameAreaTransform.offsetMax = new Vector2(-right, -top);
        gameAreaTransform.localScale = Vector3.one * scaleSize;
        gameUiBG.SetActive(true);
        showBtn.SetActive(false);
        artTxt.SetActive(false);

        //gameArea.SetActive(true);
    }
    //static void SetLeft(this RectTransform rt, float left)
    //{
    //    rt.offsetMin = new Vector2(left, rt.offsetMin.y);
    //}

    //static void SetRight(this RectTransform rt, float right)
    //{
    //    rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
    //}

    //static void SetTop(this RectTransform rt, float top)
    //{
    //    rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
    //}

    //static void SetBottom(this RectTransform rt, float bottom)
    //{
    //    rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
    //}
    public void ResetAll()
    {
        SelectionControl.instance.transform.SetParent(editUi.transform);
        SelectionControl.instance.ResetControl();
        foreach (Transform child in gameArea.transform)
        {
            Item item = child.GetComponent<Item>();
            RemoveElement(item);
            Destroy(child.gameObject);
        }
    }
    public void Home()
    {
        SaveTemplate(false);
    }
    public async void MakeFriend()
    {
        string friendId = await FirebaseData.GetUserFromTemplate(template.templateId);
        await FirebaseData.MakeFriend( gameInstanceManager.user.UserId, friendId);
    }
    #region  old
    /*public async void LoadItemForCategory(string categoryId){

        contrData = await FirebaseData.GetContainerData(gameInstanceManager.db, categoryId);
        int containerIndex = GetContainerIndex(categoryId);
        if (containerIndex != -1)
        {
            if (!itemContainer)
                return;
            foreach (Transform child in itemContainer.transform)
            {
                Destroy(child.gameObject);
            }
            itemSize = 0;
            itemContainer.sizeDelta = new Vector2(0, 0);
            // Adding Items
            string partialPath = ConstStr.FINALART + container[containerIndex] + "/";
            string fullPath = ConstStr.RESOURCESFOLDER + partialPath;
            var info = new DirectoryInfo(fullPath);
            var fileInfo = info.GetFiles();
            int j = 0;
            foreach (var file in fileInfo)
            {
                if (!file.Extension.Contains("meta") && file.Extension.Contains("png"))
                {
                    ItemElement itm = Instantiate(itemElement, itemContainer.transform).GetComponent<ItemElement>();
                    itm.CreateItem(contrData.itemData[j].isGem, partialPath + file.Name.Split('.')[0] , contrData.itemData[j].amount, categoryId + "," + j);
                    itemSize += itemElementSize;
                    itemContainer.sizeDelta = new Vector2(itemSize, 0);
                    j++;
                }
            }
        }
    }*/
    #endregion
    public async void LoadItemForCategory(string categoryId)
    {
        contrData = await FirebaseData.GetContainerData(categoryId);
        if (!itemContainer)
            return;
        foreach (Transform child in itemContainer.transform)
        {
            Destroy(child.gameObject);
        }
        itemSize = 0;
        itemContainer.sizeDelta = new Vector2(0, 0);
        for (int i = 0; i < contrData.itemData.Length; i++)
        {
            ItemElement itm = Instantiate(itemElement, itemContainer.transform).GetComponent<ItemElement>();
            itm.CreateItem(contrData.itemData[i].isGem, contrData.itemData[i].itemId, contrData.itemData[i].amount, categoryId + "," + contrData.itemData[i].itemId);
            itemSize += itemElementSize;
            itemContainer.sizeDelta = new Vector2(itemSize, 0);
        }
    }
    //int GetContainerIndex(string categoryId)
    //{
    //    for (int j = 0; j < containerNavin.Length; j++)
    //    {
    //        if (containerNavin[j] == categoryId)
    //            return j;
    //    }
    //    return -1;
    //}
    public void OpenShop()
    {
        gamePopup.OpenPopup(shopPopup);
    }
    public void ConfirmPopup()
    {
        gamePopup.OpenPopup(confirmPopup);
    }
    public void CreateDuplicateItem(Item item)
    {
        if (item.isGem)
            CreateElement(item.amount, GameCurrency.Gem, item.GetComponent<Image>().sprite, item.name);
        else
            CreateElement(item.amount, GameCurrency.Gold, item.GetComponent<Image>().sprite, item.name);
    }
    public void CreateElement(int amount, GameCurrency gameCurrency, Sprite sprite,string id){
        if(selectedItem != null){
            selectedItem.ShowControls(false);
        }
        if(gameCurrency == GameCurrency.Gem){
            if (gemsCount >= amount)
                gemsCount -= amount;
            else
            {
                gamePopup.OpenPopup(shopPopup);
                return;
            }
        }else{
            if (goldCount >= amount)
                goldCount -= amount;
            else
            {
                gamePopup.OpenPopup(shopPopup);
                return;
            }
        }
        selectedItem = Instantiate(item, gameArea.transform).GetComponent<Item>();
        UIManager.instance.UpdateUI(gemsCount,goldCount);
        selectedItem.CreateItem(gameCurrency, amount,sprite,id,true);
    }
    public void SelectItem(Item currentItem){
        if(selectedItem){
            selectedItem.ShowControls(false);
        }
        selectedItem = currentItem;
        selectedItem.ShowControls(true);
    }
    public void RemoveElement(Item deleteItem)
    {
        if (deleteItem.isGem)
            gemsCount += deleteItem.amount;
        else
            goldCount += deleteItem.amount;
        deleteItem.ShowControls(false);
        selectedItem = null;
        UIManager.instance.UpdateUI(gemsCount, goldCount);
        Destroy(deleteItem.gameObject);
    }
    public void SaveTemplate(bool makeLive)
    {
        SelectionControl.instance.Select(null, null, false);
        SelectionControl.instance.transform.SetParent(editUi.transform);
        TemplateItems[] templateItems = new TemplateItems[gameArea.transform.childCount];
        gameInstanceManager.playerData.currentGems = gemsCount;
        gameInstanceManager.playerData.currentGold = goldCount;
        int count = 0;
        foreach (Transform child in gameArea.transform)
        {
            string pos = VectorToString(child.transform.localPosition);
            RectTransform childTransform = child.GetComponent<RectTransform>();
            bool itemFlip = false;
            if (child.localScale.x == -1)
            {
                itemFlip = true;
            }
            TemplateItems i = new TemplateItems
            {
                assetId = child.name,
                position = pos,
                rotation = child.transform.localEulerAngles.z.ToString("0.00"),
                width = childTransform.rect.width,
                height = childTransform.rect.height,
                isFlip = itemFlip
            };
            templateItems[count] = i;
            count++;
        }
        template.templateItems = templateItems;
        UploadTemplate(makeLive);
    }
    async void UploadTemplate(bool makeLive)
    {
        await FirebaseData.SaveTemplate(gameInstanceManager.playerData, template, makeLive);
        LoadingManager.Instance.LoadNewScene(GameScene.Menu);
    }
    string VectorToString(Vector3 vc){
        return vc.x.ToString("0.00") + "," + vc.y.ToString("0.00") + "," + vc.z.ToString("0.00");
    }
    Vector3 StringToVector(string pos)
    {
        string[] temp = pos.Split(',');
        return new Vector3(float.Parse(temp[0]), float.Parse(temp[1]), float.Parse(temp[2]));
    }
    void LoadExistingTemplate(Transform area)
    {
        string categoryId;
        string itemId;
        string[] assetId;
        for (int i = 0; i < template.templateItems.Length; i++)
        {
            assetId = template.templateItems[i].assetId.Split(',');
            categoryId = assetId[0];
            itemId = assetId[1];
            Item itm = Instantiate(item, area).GetComponent<Item>();
            itm.CreateItem(itemId, categoryId.ToString() + "," + itemId.ToString(), false);
            itm.transform.localPosition = StringToVector(template.templateItems[i].position);
            itm.transform.rotation = Quaternion.Euler(new Vector3(0, 0, float.Parse(template.templateItems[i].rotation)));
            if (template.templateItems[i].isFlip)
            {
                itm.transform.localScale = new Vector3(-1, 1, 1);
            }
            RectTransform childTransform = itm.GetComponent<RectTransform>();
            childTransform.sizeDelta = new Vector2(template.templateItems[i].width,template.templateItems[i].height);
        }
    }
    #region old
    /*
    void LoadExistingTemplate(Transform area)
    {
        string categoryId;
        int itemId;
        string[] assetId;
        for(int i = 0; i< template.templateItems.Length; i++)
        {
            assetId = template.templateItems[i].assetId.Split(',');
            categoryId = assetId[0];
            itemId = int.Parse(assetId[1]);
            Item itm = Instantiate(item, area).GetComponent<Item>();
            int containerIndex = GetContainerIndex(categoryId);
            string partialPath = ConstStr.FINALART + container[containerIndex] + "/";
            string fullPath = ConstStr.RESOURCESFOLDER + partialPath;
            var info = new DirectoryInfo(fullPath);
            var fileInfo = info.GetFiles();
            int j = 0;
            string path = "";
            foreach (var file in fileInfo)
            {
                if (!file.Extension.Contains("meta") && file.Extension.Contains("png"))
                {
                    if(itemId == j)
                    {
                        path = partialPath + file.Name.Split('.')[0];
                        break;
                    }
                    j++;
                }
            }
            itm.CreateItem(path, categoryId.ToString() + "," + itemId.ToString(), false);
            itm.transform.localPosition = StringToVector(template.templateItems[i].position);
            itm.transform.rotation = Quaternion.Euler(new Vector3(0, 0, float.Parse(template.templateItems[i].rotation)));
            float scale = float.Parse(template.templateItems[i].scale);
            itm.transform.localScale = Vector3.one * scale;
        }
    }*/
    #endregion
    public async void LoadAllContainer()
    {
        List<string> container = await FirebaseData.GetContainerIds();
        for (int i = 0; i < container.Count; i++)
        {
            CategoryElement c = Instantiate(categoryElement, categoryContainer.transform).GetComponent<CategoryElement>();
            c.createCategory(container[i]);
            categorySize += categoryElementSize;
            categoryContainer.sizeDelta = new Vector2(categorySize, 0);
        }
        LoadItemForCategory(container[0]);
    }
    /*
    public void LoadAllContainer()
    {
        var info = new DirectoryInfo(ConstStr.RESOURCESFOLDER + ConstStr.FINALART);
        var fileInfo = info.GetFiles();
        container = new string[fileInfo.Length];
        int index = 0;
        foreach (var file in fileInfo)
        {
            container[index] = file.Name.Split('.')[0];
            index++;
        }
        for (int i =0; i< container.Length; i++)
        {
            CategoryElement c = Instantiate(categoryElement, categoryContainer.transform).GetComponent<CategoryElement>();
            c.createCategory(container[i]);
            categorySize += categoryElementSize;
            categoryContainer.sizeDelta = new Vector2(categorySize, 0);
        }
        LoadItemForCategory(container[0]);
    }
    */
    #region setup
    
  /*  void SetupFile()
    {
        //string path = "Assets/Resources/FinalArt/Airplane/";
        //var info = new DirectoryInfo(path);
        //var fileInfo = info.GetFiles();
        //int count = 0;
        ////Debug.Log("Hi");
        //foreach (var file in fileInfo)
        //{
        //    if (!file.Extension.Contains("meta"))
        //    {
        //        Debug.Log(file.Name.Split('.')[0]);
        //        count++;
        //    }

        //}
        //s[0] = Resources.Load<Sprite>("FinalArt/Airplane/Airplane");
        //Debug.Log(count);

        string temp = "Airplane,Alphabets,Animals Cartoonish,Animals Cute,animals2,arabic theme,arrows,Backgrounds,Backgrounds Modern,Banner,beach,birds,cards,cars,castle,Cats,Character Santa,charcter 1,Christmas,city,city theme,clouds,countries,crystals,decor theme,Decorative,Design Elements,Diamonds,dinosaurus,Dogs,egypt,electronic,Emoticons,Faces Only,Fair and Rides,fairy theme,farm,farm theme,feathers,festivals,fire,Fish,Flowers,Flowers Painted,Flowers Spring,food Trucks,fruits,Fruits painted,furniture,furniture hand drawn,games,Girl,ground,gun,Heroes,hills,house,House painted,ice,India,Indian Gods,insects,Kids Drawing,Kids world,kitchen,koi pond,Landscape Theme 1,Landscape Theme 2,Landscape Theme 3,Landscape Theme 4,Landscape Theme 5,Landscape Theme 6,Light,mermaid,monster world,Mountains,music,music notes,numbers,objects,Objects Colored,old theme sketch,origami,Pattern1,People Activity,people cartoon,period,Plants and Pots,playground 1,pond,Pond Theme,Pooja Flowers,pooja stuff,princess,road,rocks,room,roses,shapes,shapes simple,Shapes Theme,shapes Theme 2,ships,sketch theme forest,sky,space,speech icons,squares,stars,Sticker Words,Stickers,Story,summer theme,sun,sweet,tags,tents,theme forest,theme forest 2,toys,trees,Trees Toon,Triangles,vikings,water,water plants,waterfall,weapons,windmill,winter,wood,Words,world wonders";

        string[] tempa = temp.Split(',');
        for (int i = 0; i < container.Length; i++)
        {
            container[i] = tempa[i];
            //string path = "FinalArt/" + tempa[i] + "/";
            //string path = "Asset/Resources/FinalArt/Airplane/";
            //var info = new DirectoryInfo(path);
            //var fileInfo = info.GetFiles();
            //int count = 0;
            //foreach (var file in fileInfo)
            //{
            //    if (file.Extension.Contains("png"))
            //    {
            //        Debug.Log(count);
            //        count++;
            //    }

            //}
            //string p = path;
            //container[i].itemsPath = path;
            //foreach (FileInfo f in fileInfo)
            //{
            //    if (f.Extension == ".png")
            //    {
            //        p += f.Name + f.Extension;
            //        Debug.Log(p);
            //        //j++;
            //    }
            //}
            //int j = 0;
            //foreach (var file in fileInfo)
            //{
            //    if (file.Extension.Contains("png"))
            //    {
            //        string fileName = Path.GetFileName(file.ToString());
            //        string temp = path + fileName;
            //        Debug.Log(temp);
            //        container[i].items[j] = (Sprite)AssetDatabase.LoadAssetAtPath(temp, typeof(Sprite));
            //        j++;
            //    }
            //}
        }
    }*/
    #endregion
}
