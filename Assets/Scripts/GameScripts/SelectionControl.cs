﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SelectionControl : MonoBehaviour
{
    public static SelectionControl instance;
    [SerializeField]
    GameObject buttons;
    [SerializeField]
    Image selectionImg;
    [SerializeField]
    Button deleteBtn;
    [SerializeField]
    Button duplicateBtn;
    [SerializeField]
    Button flipBtn;
    [SerializeField]
    ItemInteraction itemInteraction;
    Item currentItem = new Item();
    bool isSelected;
    Vector3 pos;
    RectTransform myTransform;
    float spacing = 0;
    private void Awake()
    {
        instance = this;
        myTransform = GetComponent<RectTransform>();
    }
    public void Select(Item item,RectTransform imgTransform, bool isEnable)
    {
        buttons.SetActive(isEnable);
        if (isEnable)
        {
            int index = imgTransform.GetSiblingIndex();
            //transform.SetParent(GameManager.instance.gameArea.transform);
            transform.SetSiblingIndex(index + 1);
            myTransform.position = imgTransform.position;
            myTransform.rotation = imgTransform.rotation;
            myTransform.sizeDelta = new Vector2(imgTransform.rect.width + spacing, imgTransform.rect.height + spacing);
            currentItem = item;
            isSelected = true;
            itemInteraction.ScaleImage(item.GetComponent<RectTransform>());
            deleteBtn.onClick.RemoveAllListeners();
            duplicateBtn.onClick.RemoveAllListeners();
            flipBtn.onClick.RemoveAllListeners();
            deleteBtn.onClick.AddListener(delegate {
                GameManager.instance.RemoveElement(currentItem);
            });
            duplicateBtn.onClick.AddListener(delegate {
                GameManager.instance.CreateDuplicateItem(currentItem);
            });
            flipBtn.onClick.AddListener(delegate {
                if (currentItem.transform.localScale.x == -1)
                    currentItem.transform.localScale = Vector3.one;
                else
                    currentItem.transform.localScale = new Vector3(-1, 1, 1);
            });
        }
        else
        {
            
            isSelected = true;
        }
        selectionImg.enabled = isEnable;
    }
    public void ResetControl()
    {
        buttons.SetActive(false);
        selectionImg.enabled = false;
    }
    
}
