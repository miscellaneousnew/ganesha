﻿using Firebase.Database;
using Firebase.Storage;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

public static class FirebaseData
{
    public static async Task<string> GetPrivacy()
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var privacy = await db.Child(ConstStr.PRIVACY).GetValueAsync();
        return privacy.Value.ToString();
    }
    public static async Task<string> GetConditions()
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var terms = await db.Child(ConstStr.TANDC).GetValueAsync();
        return terms.Value.ToString();
    }
    public static async Task<List<MyTemplates>> GetUserTemplates(string userId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var myTemplates = await db.Child(ConstStr.PLAYERS + userId + ConstStr.MYTEMPLATES).GetValueAsync();
        Debug.Log(userId);
        var mybestLive = await db.Child(ConstStr.BESTTEMPLATES + userId).GetValueAsync();
        var myspecialLive = await db.Child(ConstStr.SPECIALTEMPLATES + userId).GetValueAsync();
        var mynewLive = await db.Child(ConstStr.NEWTEMPLATES + userId).GetValueAsync();
        List<MyTemplates> task = new List<MyTemplates>();
        foreach (DataSnapshot myTemplate in myTemplates.Children)
        {
            var template = await db.Child(ConstStr.TEMPLATERS).Child(myTemplate.Key).GetValueAsync();
            Templates t = JsonUtility.FromJson<Templates>(template.GetRawJsonValue());
            bool isLive = false;
            
            if (mybestLive.Value != null && t.templateId == mybestLive.Value.ToString())
            {
                isLive = true;
            }
            if(myspecialLive.Value !=null && t.templateId == myspecialLive.Value.ToString()){
                isLive = true;
            }
            if(mynewLive.Value != null && t.templateId == mynewLive.Value.ToString()){
                isLive = true;
            }
            if (t.inReview == true)
            {
                isLive = true;
            }
            if (t.challengeId == "")
            {
                MyTemplates temp = new MyTemplates()
                {
                    templateId = t.templateId,
                    templateName = t.templateName,
                    isLive = isLive
                };
                task.Add(temp);
            }
            //if(t.inReview == true)
            //{
            //    MyTemplates temp = new MyTemplates()
            //    {
            //        templateId = t.templateId,
            //        templateName = t.templateName
            //    };
            //    task.Add(temp);
            //}
        }
        return task;
    }
    public static async Task<WinFree> GetReward()
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var winFree =  await db.Child(ConstStr.WINFREE).GetValueAsync();
        WinFree free = JsonUtility.FromJson<WinFree>(winFree.GetRawJsonValue());
        return free;
    } 
    public static async Task<List<CompletedChallengeTemplates>> GetUserCompletedTemplates(string userId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var myTemplates = await db.Child(ConstStr.PLAYERS + userId + ConstStr.MYTEMPLATES).GetValueAsync();
        List<CompletedChallengeTemplates> task = new List<CompletedChallengeTemplates>();
        foreach (DataSnapshot myTemplate in myTemplates.Children)
        {
            var template = await db.Child(ConstStr.TEMPLATERS).Child(myTemplate.Key).GetValueAsync();
            Templates t = JsonUtility.FromJson<Templates>(template.GetRawJsonValue());
            if(t.challengeId != "")
            {
                if (t.inReview || t.isCompleted)
                {
                    CompletedChallengeTemplates completeTemplate = new CompletedChallengeTemplates();
                    completeTemplate.templateName = t.templateName;
                    completeTemplate.inReview = t.inReview;
                    completeTemplate.rating = t.rating;
                    string challengeId = t.challengeId;
                    var challengeGem = await db.Child(ConstStr.CHALLENGES).Child(challengeId).Child("gemAmount").GetValueAsync();
                    var challengeCoin = await db.Child(ConstStr.CHALLENGES).Child(challengeId).Child("coinAmount").GetValueAsync();
                    completeTemplate.gemAmount = int.Parse(challengeGem.Value.ToString());
                    completeTemplate.coinAmount = int.Parse(challengeCoin.Value.ToString());
                    task.Add(completeTemplate);
                }
            }
        }
        return task;
    }
    public static async Task<Templates> GetTemplate(string templateId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var template = await db.Child(ConstStr.TEMPLATERS + templateId).GetValueAsync();
        Templates tasks = JsonUtility.FromJson<Templates>(template.GetRawJsonValue());
        return tasks;
    }
    public static async Task<string> GetUserName(string userId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var name = await db.Child(ConstStr.PLAYERS + userId).Child(ConstStr.PLAYERNAME).GetValueAsync();
        return name.Value.ToString();
    }
    public static async Task SaveTemplate(PlayerData playerData,Templates template, bool makeLive)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        Debug.Log(makeLive);
        if (makeLive)
        {
            template.inReview = true;
        }
        else
        {
            template.inReview = false;
        }
        await db.Child(ConstStr.TEMPLATERS + template.templateId).SetRawJsonValueAsync(JsonUtility.ToJson(template));
        await db.Child(ConstStr.PLAYERS + playerData.playerId).Child(ConstStr.CURRENTGEMS).SetValueAsync(playerData.currentGems);
        await db.Child(ConstStr.PLAYERS + playerData.playerId).Child(ConstStr.CURRENTGOLD).SetValueAsync(playerData.currentGold);
        string templateName = "";
        if(template.challengeId != null)
        {
            templateName = template.challengeId;
        }
        else {
            templateName = template.templateName;
        }
        Dictionary<string, object> childUpdates = new Dictionary<string, object>
        {
            [playerData.playerId + ConstStr.MYTEMPLATES + "/" + template.templateId] = templateName
        };
        await db.Child(ConstStr.PLAYERS).UpdateChildrenAsync(childUpdates);
        if (makeLive)
        {
            await MakeTemplateLive(playerData.playerId, template.templateId);
        }
    }
    public static async void CreateNewUser(string userId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        PlayerData pd = new PlayerData(userId, 100000, 10000);
        await db.Child(ConstStr.PLAYERS + userId).SetRawJsonValueAsync(JsonUtility.ToJson(pd));
    }
    public static async Task<PlayerData> FetchPlayerData(string userId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var Player = await db.Child(ConstStr.PLAYERS + userId).GetValueAsync();
        PlayerData pd = JsonUtility.FromJson<PlayerData>(Player.GetRawJsonValue());
        pd.playerId = userId;
        return pd;
        //var gold = await db.Child(ConstStr.PLAYERS + userId).Child(ConstStr.CURRENTGOLD).GetValueAsync();
        //Debug.Log(userId + "YYYYYYY");
        //Debug.Log(gems.Value + "PPP");
        //Debug.Log(gold.Value);
        //return new PlayerData(userId, int.Parse(gems.Value.ToString()), int.Parse(gold.Value.ToString()));
    }
    public static async Task UpdatePlayerData( PlayerData playerData)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        await db.Child(ConstStr.PLAYERS + playerData.playerId).Child(ConstStr.CURRENTGEMS).SetValueAsync(playerData.currentGems);
        await db.Child(ConstStr.PLAYERS + playerData.playerId).Child(ConstStr.CURRENTGOLD).SetValueAsync(playerData.currentGold);
    }
    public static async Task MakeTemplateLive( string userId, string templateId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        await db.Child(ConstStr.TEMPLATERS + templateId).Child("inReview").SetValueAsync(true);
        //await db.Child(ConstStr.PLAYERSLIVETEMPLATE).Child(userId).SetValueAsync(templateId);
    }
    public static async Task<Dictionary<string,string>> FetchNotification(string userId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var notify = await db.Child(ConstStr.NOTIFICATION).GetValueAsync();
        var myNotify = await db.Child(ConstStr.PLAYERS + userId).Child(ConstStr.MYNOTIFICATION).GetValueAsync();
        Dictionary<string,string> newMsg = new Dictionary<string,string>();
        foreach (DataSnapshot msg in notify.Children)
        {
            newMsg.Add(msg.Key, msg.Value.ToString());
        }
        foreach (DataSnapshot msg in myNotify.Children)
        {
            newMsg.Remove(msg.Key);
        }
        return newMsg;
    }
    public static async void UpdateNotification(Dictionary<string,string> newMsg, string userId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        if (newMsg.Count > 0)
        {
            Dictionary<string, object> childUpdates = new Dictionary<string, object>
            {
                [userId + ConstStr.MYNOTIFICATION + "/"] = newMsg
            };
            await db.Child(ConstStr.PLAYERS).UpdateChildrenAsync(childUpdates);
        }
    }
    public static async Task MakeFriend(string userId, string friendId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        Dictionary<string, object> childUpdates = new Dictionary<string, object>
        {
            [userId + ConstStr.MYFRIENDS + "/" + friendId] = true
        };
        await db.Child(ConstStr.PLAYERS).UpdateChildrenAsync(childUpdates);
    }
    public static async Task<string> GetUserFromTemplate(string templateId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var users = await db.Child(ConstStr.PLAYERSLIVETEMPLATE).GetValueAsync();
        string userId = "";
        foreach(DataSnapshot user in users.Children)
        {
            if(user.Value.ToString() == templateId)
            {
                userId = user.Key;
            }
        }
        return userId;
    }
    public static async Task<List<Friend>> GetFriendsList(string userId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var friendList = await db.Child(ConstStr.PLAYERS).Child(userId).Child(ConstStr.MYFRIENDS).GetValueAsync();
        List<Friend> tasks = new List<Friend>();
        foreach (DataSnapshot friend in friendList.Children)
        {
            await Task.Run(async () =>
            {
                var getName = await db.Child(ConstStr.PLAYERS).Child(friend.Key).Child(ConstStr.PLAYERNAME).GetValueAsync();
                var getTemplateId = await db.Child(ConstStr.PLAYERSLIVETEMPLATE).Child(friend.Key).GetValueAsync();
                var name = getName.Value.ToString();
                var templateId = getTemplateId.Value.ToString();
                var Friend = new Friend(friend.Key, name, templateId);
                tasks.Add(Friend);
            });

        }
        return tasks;
    }
    public static async Task<List<Challenges>> GetChallenges(string userId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var ChallengesList = await db.Child(ConstStr.CHALLENGES).GetValueAsync();
        var myTemplates = await db.Child(ConstStr.PLAYERS + userId + ConstStr.MYTEMPLATES).GetValueAsync();
        List<Challenges> tasks = new List<Challenges>();
        foreach (DataSnapshot challenge in ChallengesList.Children)
        {
            Challenges c = JsonUtility.FromJson<Challenges>(challenge.GetRawJsonValue());
            tasks.Add(c);
        }
        foreach (DataSnapshot myTemp in myTemplates.Children)
        {
            string challengeId = myTemp.Value.ToString();
            for (int i = 0; i < tasks.Count; i++)
            {
                if(tasks[i].challengeId == challengeId)
                {
                    var temp = await db.Child(ConstStr.TEMPLATERS).Child(myTemp.Key).GetValueAsync();
                    Templates template = JsonUtility.FromJson<Templates>(temp.GetRawJsonValue());
                    if(template.isCompleted)
                        tasks.Remove(tasks[i]);
                    if(template.inReview)
                        tasks.Remove(tasks[i]);
                }
            }
        }
        return tasks;
    }
    public static async Task<Challenges> GetChallenge(string challengeId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var ChallengesList = await db.Child(ConstStr.CHALLENGES).GetValueAsync();
        Challenges c = new Challenges();
        foreach (DataSnapshot challenge in ChallengesList.Children)
        {
            c = JsonUtility.FromJson<Challenges>(challenge.GetRawJsonValue());
            if(c.challengeId == challengeId)
            {
                break;
            }
        }
        return c;
    }
    public static async Task<string> GetMyExistingChallenge(string userId,string challengeId)
    {
        string templateId = "";
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var myTemplates = await db.Child(ConstStr.PLAYERS + userId + ConstStr.MYTEMPLATES).GetValueAsync();
        List<MyTemplates> task = new List<MyTemplates>();
        foreach (DataSnapshot myTemplate in myTemplates.Children)
        {
            if(myTemplate.Value.ToString() == challengeId)
            {
                templateId = myTemplate.Key;
            }
        }
        return templateId;
    }
    public static async Task<Texture2D> GetProfilePic(FirebaseStorage storage, string id)
    {
        Texture2D pic = new Texture2D(2, 2);
        byte[] data = null;
        StorageReference reference = storage.GetReference(ConstStr.PROFILEPIC + id + ".jpg");
        const long maxAllowedSize = 1 * 1024 * 1024;
        await Task.Run(async () =>
        {
            await reference.GetBytesAsync(maxAllowedSize).ContinueWith((Task<byte[]> task) =>
            {
                if (task.IsFaulted || task.IsCanceled)
                {
                    Debug.Log(task.Exception.ToString());
                    // Uh-oh, an error occurred!
                }
                else
                {
                    data = task.Result;

                    //Debug.Log("Finished downloading!");
                }
            });
        });
        pic.LoadImage(data);
        return pic;
    }
    public static async Task<Texture2D> GetChallengeImg(FirebaseStorage storage, string imgUrl)
    {
        Texture2D pic = new Texture2D(2, 2);
        byte[] data = null;
        StorageReference reference = storage.GetReference(ConstStr.CHALLENGEPIC + imgUrl);
        const long maxAllowedSize = 1 * 2048 * 2048;
        await Task.Run(async () =>
        {
            await reference.GetBytesAsync(maxAllowedSize).ContinueWith((Task<byte[]> task) =>
            {
                if (task.IsFaulted || task.IsCanceled)
                {
                    Debug.Log(task.Exception.ToString());
                    // Uh-oh, an error occurred!
                }
                else
                {
                    data = task.Result;

                    //Debug.Log("Finished downloading!");
                }
            });
        });
        pic.LoadImage(data);
        return pic;
    }
    public static async Task<Texture2D> UploadProfilePic(Texture2D picTexture, FirebaseStorage storage,string userId)
    {
        byte[] pic = duplicateTexture(picTexture).EncodeToPNG();
        var metadata = new MetadataChange();
        metadata.ContentType = "image/jpeg";
        StorageReference photo_ref = storage.GetReference(ConstStr.PROFILEPIC).Child("/" + userId + ".jpg");
        await photo_ref.PutBytesAsync(pic, metadata).ContinueWith((Task<StorageMetadata> task) => {
            if (task.IsFaulted || task.IsCanceled)
            {
                Debug.Log(task.Exception.ToString());
                // Uh-oh, an error occurred!
            }
            else
            {
                Debug.Log("File Uploaded");
            }
        });
        return picTexture;
    }
    static Texture2D duplicateTexture(Texture2D source){
        RenderTexture renderTex = RenderTexture.GetTemporary(source.width,source.height, 0, RenderTextureFormat.Default, RenderTextureReadWrite.Linear);
        Graphics.Blit(source, renderTex);
        RenderTexture previous = RenderTexture.active;
        RenderTexture.active = renderTex;
        Texture2D readableText = new Texture2D(source.width, source.height);
        readableText.ReadPixels(new Rect(0, 0, renderTex.width, renderTex.height), 0, 0);
        readableText.Apply();
        RenderTexture.active = previous;
        RenderTexture.ReleaseTemporary(renderTex);
        return readableText;
    }
    public static async Task<List<Store>> GetStoreList()
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var storeList = await db.Child(ConstStr.STORE).GetValueAsync();
        List<Store> tasks = new List<Store>();
        foreach (DataSnapshot str in storeList.Children)
        {
            Store s = JsonUtility.FromJson<Store>(str.GetRawJsonValue());
            tasks.Add(s);
        }
        return tasks;
    }
    public static async Task<List<LiveTemplate>> GetTemplateList(string templateFilter)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var templates = await db.Child(templateFilter).GetValueAsync();
        List<LiveTemplate> tasks = new List<LiveTemplate>();
        foreach (DataSnapshot t in templates.Children)
        {
            await Task.Run(async () =>
            {
                string templateId = t.Value.ToString();
                var temp = await db.Child(ConstStr.TEMPLATERS).Child(templateId).GetValueAsync();
                Templates temp1 = JsonUtility.FromJson<Templates>(temp.GetRawJsonValue());
                if(temp1.inReview == false)
                {
                    var getName = await db.Child(ConstStr.PLAYERS).Child(t.Key).Child(ConstStr.PLAYERNAME).GetValueAsync();
                    //var getTemplateId = await db.Child(ConstStr.PLAYERSLIVETEMPLATE).Child(t.Key).GetValueAsync();
                    var name = getName.Value.ToString();
                    //var templateId = getTemplateId.Value.ToString();
                    var template = new LiveTemplate(t.Key, name, templateId);
                    tasks.Add(template);
                }
                
            });
        }
        return tasks;
    }
    //public static async Task<List<LiveTemplate>> GetAllTemplateList(DatabaseReference db)
    //{
    //    var templates = await db.Child(ConstStr.PLAYERSLIVETEMPLATE).GetValueAsync();
    //    List<LiveTemplate> tasks = new List<LiveTemplate>();
    //    foreach (DataSnapshot t in templates.Children)
    //    {
    //        await Task.Run(async () =>
    //        {
    //            var getName = await db.Child(ConstStr.PLAYERS).Child(t.Key).Child(ConstStr.PLAYERNAME).GetValueAsync();
    //            var name = getName.Value.ToString();
    //            var template = new LiveTemplate(t.Key, name, t.Value.ToString());
    //            tasks.Add(template);
    //        });
    //    }
    //    return tasks;
    //}
    public static async Task<ContainerData> GetContainerData(string containerId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var containerTemplate = await db.Child(ConstStr.CONTAINER).Child(containerId).GetValueAsync();
        ContainerData container = JsonUtility.FromJson<ContainerData>(containerTemplate.GetRawJsonValue());
        return container;
    }
    public static async Task<List<string>> GetContainerIds()
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var containers = await db.Child(ConstStr.CONTAINER).GetValueAsync();
        List<string> tasks = new List<string>();
        foreach (DataSnapshot t in containers.Children)
        {
            tasks.Add(t.Key);
        }
        return tasks;
    }
    public static async Task<List<string>> GetProductIds ()
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var store = await db.Child(ConstStr.STORE).GetValueAsync();
        List<string> tasks = new List<string>();
        foreach (DataSnapshot t in store.Children)
        {
            Store s = JsonUtility.FromJson<Store>(t.GetRawJsonValue());
            tasks.Add(s.id);
        }
        return tasks;
    }
    public static async Task<ItemPurchased> GetItemForId(string productId)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var store = await db.Child(ConstStr.STORE).GetValueAsync();
        ItemPurchased tasks = new ItemPurchased();
        foreach (DataSnapshot t in store.Children)
        {
            Store s = JsonUtility.FromJson<Store>(t.GetRawJsonValue());
            if(s.id == productId)
            {
                tasks.isGem = s.isGem;
                tasks.amount = s.quantity;
                break;
            }
        }
        return tasks;
    }
    #region Admin
    public static async Task<List<Templates>> GetTemplatesForReview()
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        var reviewTemp = await db.Child(ConstStr.PLAYERSLIVETEMPLATE).GetValueAsync();
        List<Templates> tasks = new List<Templates>();
        foreach(DataSnapshot d in reviewTemp.Children)
        {
            await Task.Run(async () =>
            {
                var getTemplate = await db.Child(ConstStr.TEMPLATERS).Child(d.Value.ToString()).GetValueAsync();
                Templates temp = JsonUtility.FromJson<Templates>(getTemplate.GetRawJsonValue());
                if(!temp.isCompleted)
                    tasks.Add(temp);
            });
        }
        return tasks;
    }
    public static async Task ReviewTemplate( Templates template ,float rating, string reviewType, int gemsReward , int coinsReward,bool isFeature)
    {
        DatabaseReference db = FirebaseDatabase.DefaultInstance.RootReference;
        template.rating = rating;
        template.inReview = false;
        template.isCompleted = true;
        int gem = gemsReward;
        int coin = coinsReward;
        if(template.challengeId != "")
        {
            Challenges c = await GetChallenge(template.challengeId);
            if (rating == 0)
            {
                gem = 0;
                coin = 0;
            }
            else
            {
                gem = (int) ((c.gemAmount/ 5) * rating);
                coin = (int) ((c.coinAmount / 5) * rating);
            }
        }
        PlayerData pd = await FetchPlayerData(template.playerId);
        pd.currentGems += gem;
        pd.currentGold += coin;
        await UpdatePlayerData(pd);
        Dictionary<string, object> childUpdates = new Dictionary<string, object>
        {
            [template.playerId] = template.templateId
        };
        if(isFeature)
            await db.Child(reviewType).UpdateChildrenAsync(childUpdates);
        await db.Child(ConstStr.TEMPLATERS + template.templateId).SetRawJsonValueAsync(JsonUtility.ToJson(template));
    }
    #endregion
}
