﻿using System.Collections;
using System.Collections.Generic;
using Firebase.Database;
using UnityEngine;
using Firebase;
using Firebase.Storage;
using Firebase.Auth;
using System;
using System.Threading.Tasks;
using UnityEngine.UI;

public delegate void FetchDelegate(DataSnapshot data);
public delegate void UpdateDelegate(bool isSuccess);
public class GameInstanceManager : MonoBehaviour
{
    //public static event Action<Texture2D> uploadProfile;
    public static event Action purchaseMade;
    public static event Action insufficientCurreny;
    private static GameInstanceManager instance;
    public PlayerData playerData;
    public DatabaseReference db;
    public FirebaseUser user;
    public FirebaseStorage storage;
    FirebaseApp app;

    //Texture2D myProfilePic;
    Dictionary<string, Texture2D> profilePics = new Dictionary<string, Texture2D>();
    Dictionary<string, Texture2D> challengePic = new Dictionary<string, Texture2D>();
    public bool isTemplate;
    public Templates currentTemplate;
    [SerializeField]
    public Challenges challenge;
    public bool isDisplay;
    public bool isChallenge;

    public static GameInstanceManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<GameInstanceManager>();
                if (instance == null)
                {
                    instance = new GameObject("Game Instance", typeof(GameInstanceManager)).GetComponent<GameInstanceManager>();

                }
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
    }
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    public void Setup()
    {
        app = AuthenticationManager.instance.app;
        db = AuthenticationManager.instance.db;
        user = AuthenticationManager.instance.user;
        storage = FirebaseStorage.GetInstance(app, ConstStr.STORAGE);
    }
    // Update is called once per frame
    //void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.U))
    //    {
    //        //PlayerData pd = new PlayerData(user.UserId);
    //        //db.Child(ConstStr.PLAYERS + user.UserId).SetRawJsonValueAsync(JsonUtility.ToJson(pd));
    //    }
    //    if (Input.GetKeyDown(KeyCode.Y))
    //    {
    //        //PlayerData pd = new PlayerData(user.UserId);
    //        //db.Child(ConstStr.PLAYERS + user.UserId).SetRawJsonValueAsync(JsonUtility.ToJson(pd));
    //        LoadData();
    //    }
    //}
    //void LoadData()
    //{
    //    FirebaseData.CreateNewUser(db,user.UserId);
    //    Debug.Log("Done");
    //}

    #region Profile Pic
    public async void GetProfileImage(RawImage img, string id)
    {
        if (profilePics.ContainsKey(id))
        {
            img.texture = profilePics[id];
        }
        else
        {
            Texture2D profileTex = await FirebaseData.GetProfilePic(storage, id);
            if(profileTex != null && img != null)
            {
                img.texture = profileTex;
                if (!profilePics.ContainsKey(id))
                    profilePics.Add(id, profileTex);
            }
        }
    }
    public async Task<Texture2D> GetImage(string imgUrl)
    {
        Texture2D texture = new Texture2D(2, 2);
        if (challengePic.ContainsKey(imgUrl))
        {
            texture = challengePic[imgUrl];
        }
        else
        {
            texture = await FirebaseData.GetChallengeImg(storage, imgUrl);
            if (texture != null)
            {
                if (!challengePic.ContainsKey(imgUrl))
                    challengePic.Add(imgUrl, texture);
            }
        }
        return texture;
    }
    //IEnumerator GetProfilePic(Transform img, string id)
    //{
    //    bool isFileReady = false;
    //    if (profilePhoto == null)
    //    {
    //        StorageReference reference = storage.GetReference(ConstStr.PROFILEPIC + id + ".jpg");
    //        const long maxAllowedSize = 1 * 1024 * 1024;
    //        reference.GetBytesAsync(maxAllowedSize).ContinueWith((Task<byte[]> task) => {
    //            if (task.IsFaulted || task.IsCanceled)
    //            {
    //                Debug.Log(task.Exception.ToString());
    //                // Uh-oh, an error occurred!
    //            }
    //            else
    //            {
    //                profilePhoto = task.Result;
    //                isFileReady = true;
    //                //Debug.Log("Finished downloading!");
    //            }
    //        });
    //    }
    //    else
    //    {
    //        isFileReady = true;
    //    }
    //    yield return new WaitUntil(() => isFileReady == true);
    //    Texture2D texture = new Texture2D(2, 2);
    //    texture.LoadImage(profilePhoto);
    //    img.GetComponent<RawImage>().texture = texture;
    //}
    //public async void UploadProfileImg(Texture2D pic)
    //{
    //    //StartCoroutine(UploadProfilePic(pic));

    //    uploadProfile?.Invoke(pic);
    //}

    #endregion
    public async Task<string> GetUsername()
    {
       return await FirebaseData.GetUserName(user.UserId);
    }

    public async void LoadTemplate(string id = "", bool isChallenge = false)
    {
        if (id != "")
        {
            isTemplate = true;
            
            currentTemplate = await FirebaseData.GetTemplate(id);
            currentTemplate.playerId = user.UserId;
        }
        else{
            isTemplate = false;
            currentTemplate = GetNewTemplate(challenge.title1);
            currentTemplate.challengeId = challenge.challengeId;
            currentTemplate.playerId = user.UserId;
        }
        isDisplay = false;
        this.isChallenge = isChallenge;
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        LoadingManager.Instance.LoadNewScene(GameScene.Game);
    }

    public async void CreateNewTemplate(string templateName)
    {
        if(playerData.currentGems >= 10)
        {
            currentTemplate = GetNewTemplate(templateName);
            currentTemplate.playerId = user.UserId;
            isTemplate = true;
            playerData.currentGems -= 10;
            await FirebaseData.UpdatePlayerData( playerData);
            Screen.orientation = ScreenOrientation.LandscapeLeft;
            LoadingManager.Instance.LoadNewScene(GameScene.Game);
        }
        else
        {
            insufficientCurreny?.Invoke();
        }
    }
    Templates GetNewTemplate(string templateName)
    {
        string id = Guid.NewGuid().ToString("N");
        return new Templates
        {
            templateId = id,
            templateName = templateName,
            inReview = true,
            rating = 0,
            templateItems = new TemplateItems[0]
        };
    }
    public async void MakeTemplateLive(string id)
    {
        await FirebaseData.MakeTemplateLive(user.UserId, id);
    }
    public async void DisplayTemplate(string id)
    {
        Templates t = await FirebaseData.GetTemplate(id);
        currentTemplate = t;
        isTemplate = true;
        isDisplay = true;
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        LoadingManager.Instance.LoadNewScene(GameScene.Game);
    }
    public async void ItemPurchased(string productId)
    {
        ItemPurchased item = await FirebaseData.GetItemForId(productId);
        if (item.isGem)
        {
            playerData.currentGems += item.amount;
        }
        else
        {
            playerData.currentGold += item.amount;
        }
        await FirebaseData.UpdatePlayerData(playerData);
        purchaseMade?.Invoke();
    }
}
