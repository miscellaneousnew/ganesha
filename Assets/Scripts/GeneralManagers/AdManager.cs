﻿using UnityEngine;
using GoogleMobileAds.Api;
using System;

public class AdManager : MonoBehaviour
{
   private static AdManager instance;
   private string appID = "";
   private string interstitialID = "";
   private string videoID = "";
   public static event Action InterstitialAdClosed;
   public static event Action RewardedVideoAdClosed;
   public static event Action RewardTrigger;
   public static AdManager Instance
   {
       get
       {
           if (instance == null)
           {
               instance = FindObjectOfType<AdManager>();
               if (instance == null)
               {
                   instance = new GameObject("AdManager", typeof(AdManager)).GetComponent<AdManager>();
               }
           }
           return instance;
       }
       private set
       {
           instance = value;
       }
   }
   private InterstitialAd interstitial;
   private RewardedAd rewardedAd;
   private BannerView bannerView;
   void Awake()
   {
       DontDestroyOnLoad(gameObject);
       SetUpIDs();
       //MobileAds.Initialize(appID);
       //ShowBannerAd();
       LoadInterstitial();
       LoadRewardedVideo();
   }
   void ShowBannerAd()
   {
       if (bannerView != null)
       {
           bannerView.Destroy();
       }
       string adUnitId = "ca-app-pub-3940256099942544/6300978111";
       //bannerView = new BannerView(adUnitId, AdSize.Banner,0,50);
       AdSize adaptiveSize = AdSize.GetCurrentOrientationAnchoredAdaptiveBannerAdSizeWithWidth(AdSize.FullWidth);

       bannerView = new BannerView(adUnitId, adaptiveSize, AdPosition.Bottom);
       AdRequest adRequest = new AdRequest.Builder()
           .AddTestDevice(AdRequest.TestDeviceSimulator)
           .AddTestDevice("0123456789ABCDEF0123456789ABCDEF")
           .Build();

       // Load a banner ad.
       bannerView.LoadAd(adRequest);
   }
   void SetUpIDs()
   {
#if UNITY_ANDROID
       appID = "ca-app-pub-3940256099942544/1033173712";
       interstitialID = "ca-app-pub-3940256099942544/1033173712";
       videoID = "ca-app-pub-3940256099942544/5224354917";
#elif UNITY_IPHONE
       appID = "ca-app-pub-3940256099942544/4411468910";
       videoID = "ca-app-pub-4196507429707592/1793676595";
#else
       appID = "unexpected_platform";
       videoID = "ca-app-pub-4196507429707592/1793676595";
#endif
   }
   public void ShowRewardedVideo()
   {
       if (rewardedAd.IsLoaded())
       {
           rewardedAd.Show();
       }
   }
   public void ShowInterstitial()
   {
       if (interstitial.IsLoaded())
       {
           interstitial.Show();
       }
   }
   void LoadInterstitial()
   {
       interstitial = new InterstitialAd(interstitialID);
       interstitial.OnAdClosed += HandleOnAdClosed;
       AdRequest request = new AdRequest.Builder().Build();
       interstitial.LoadAd(request);
   }
   void HandleOnAdClosed(object sender, EventArgs args)
   {
       if (interstitial != null)
       {
           interstitial.Destroy();
       }
       InterstitialAdClosed?.Invoke();
       LoadInterstitial();
   }
   void LoadRewardedVideo()
   {
       rewardedAd = new RewardedAd(videoID);
       rewardedAd.OnUserEarnedReward += HandleUserEarnedReward;
       rewardedAd.OnAdClosed += HandleRewardedAdClosed;
       AdRequest request = new AdRequest.Builder().Build();
       rewardedAd.LoadAd(request);
   }
   public void HandleUserEarnedReward(object sender, Reward args)
   {
       RewardTrigger?.Invoke();
   }
   public void HandleRewardedAdClosed(object sender, EventArgs args)
   {
       RewardedVideoAdClosed?.Invoke();
       LoadRewardedVideo();
   }
}
