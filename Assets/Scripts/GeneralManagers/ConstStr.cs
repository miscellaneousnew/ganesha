using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public class ConstStr : MonoBehaviour
{
    private static ConstStr instance;
    public const string FIREBASEDB = "https://ganesha-f07e3.firebaseio.com/";
    public const string STORAGE = "gs://ganesha-f07e3.appspot.com";
    public const string PLAYERS = "Players/";
    public const string STORE = "Store/";
    public const string CHALLENGES = "Challenges/";
    public const string BESTTEMPLATES = "LiveTemplates/BestTemplates/";
    public const string SPECIALTEMPLATES = "LiveTemplates/SpecialTemplates/";
    public const string NEWTEMPLATES = "LiveTemplates/NewTemplates/";
    public const string MYTEMPLATES = "/myTemplates";
    public const string WINFREE = "WinFree";
    public const string NOTIFICATION = "Notification";
    public const string MYFRIENDS = "/myFriends";
    public const string MYNOTIFICATION = "/myNotification";
    public const string TEMPLATERS = "Templates/";
    public const string PLAYERSLIVETEMPLATE = "PlayerLiveTemplate/";
    public const string CONTAINER = "Container/";
    public const string APPSETTINGS = "AppSettings/";
    public const string LEVELDATA = "LevelData/";
    public const string PROFILEPIC = "/ProfilePic/";
    public const string CHALLENGEPIC = "/ChallengePic/";
    public const string SCENEPATH = "Assets/Scenes/";
    public const string PLAYERNAME = "playerName";
    public const string PLAYERID = "playerId";
    public const string CURRENTGEMS = "currentGems";
    public const string CURRENTGOLD = "currentGold";
    public const string FINALART = "FinalArt/";
    public const string RESOURCESFOLDER = "Assets/Resources/";
    public const string PRIVACY = "Privacy";
    public const string TANDC = "TermsConditions";
    public static string GenerateName(int len)
    {
        Random r = new Random();
        string[] consonants = { "b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "l", "n", "p", "q", "r", "s", "sh", "zh", "t", "v", "w", "x" };
        string[] vowels = { "a", "e", "i", "o", "u", "ae", "y" };
        string Name = "";
        Name += consonants[r.Next(consonants.Length)].ToUpper();
        Name += vowels[r.Next(vowels.Length)];
        int b = 2; //b tells how many times a new letter has been added. It's 2 right now because the first two letters are already in the name.
        while (b < len)
        {
            Name += consonants[r.Next(consonants.Length)];
            b++;
            Name += vowels[r.Next(vowels.Length)];
            b++;
        }

        return Name;


    }
}

public enum GameCurrency{
    Gem,
    Gold
}