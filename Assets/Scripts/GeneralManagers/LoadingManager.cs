﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System;

public enum GameScene
{
    Loading,
    Menu,
    Game
}
public class LoadingManager : MonoBehaviour
{
    private static LoadingManager instance;


    //[SerializeField]
    //GameObject VerificationPanel;
    //[SerializeField]
    //TMP_InputField mobileInput;
    //[SerializeField]
    //TMP_InputField verificationCodeInput;
    public static event Action DownloadComplete;
    public static event Action<float> progress;
    public bool isClearCache;
    [SerializeField]
    AssetBundle[] mBundle;
    string verificationId;
    [SerializeField]
    string[] mUri;
    public bool isActiveUser;
    public bool isAssetReady;
    public static LoadingManager Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<LoadingManager>();
                if (instance == null)
                {
                    instance = new GameObject("LoadingManager", typeof(LoadingManager)).GetComponent<LoadingManager>(); 
                }
            }
            return instance;
        }
        private set
        {
            instance = value;
        }
    }
    GameInstanceManager gameInstanceManager;
    void Awake()
    {
        instance = this;
        //for (int i = 0; i < clouds.Length; i++)
        //{
        //    LeanTween.moveLocalX(clouds[i].gameObject, cloudsTranforms[i].position.x, .5f).setRepeat(-1);
        //}
        mBundle = new AssetBundle[mUri.Length];
        Caching.compressionEnabled = false;
        gameInstanceManager = GameInstanceManager.Instance;
        AuthenticationManager.FirebaseTrigger += LoadMenu;
        StartCoroutine(GetBundle());
        //AuthenticationManager.FirebasePhoneTrigger += MobileVerificationPopup;
        DontDestroyOnLoad(gameObject);
    }
    void LoadMenu(bool isActiveUser)
    {
        this.isActiveUser = isActiveUser;
        //Debug.Log(isActiveUser);
        GameInstanceManager.Instance.Setup();
        //if (isActiveUser)
        //{
        //    //Debug.Log(AuthenticationManager.instance.auth.CurrentUser.UserId);
        //    //StartCoroutine(LoadMenu());
        //    //popups.OpenCloseCreateBtnArea(false);

        //    //LoadNewScene(GameScene.Menu);
        //}
        //else
        //{
        //    //LoadNewScene(GameScene.Login);
        //    //if (mBundle[mBundle.Length - 1] != null)
        //    //{
        //    //    popups.OpenCloseCreateBtnArea(true);
        //    //}
        //}
    }
    
    //public void MobileVerificationPopup(bool isOpen)
    //{
    //    if (isOpen)
    //    {
    //        VerificationPanel.SetActive(true);
    //    }
    //    else
    //    {
    //        VerificationPanel.SetActive(false);
    //    }
    //}
    public void LoadNewScene(GameScene sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName.ToString());
    }
    public void LoadNewScene(string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }
    //public void GetCode()
    //{
    //    if (mobileInput.text != "")
    //        AuthenticationManager.instance.PhoneVerification(mobileInput.text);
    //}
    public void VerifyBtn()
    {
        //AuthenticationManager.instance.StartMobileVerification(verificationCodeInput.text);
    }

    //IEnumerator GetBundle()
    //{
    //    WWW request = WWW.LoadFromCacheOrDownload(mUri,0);
    //    int percent;
    //    while (!request.isDone)
    //    {
    //        percent = (int)(request.progress * 100);
    //        progressTxt.text = percent.ToString();
    //        yield return null;
    //    }
    //    if(request.error == null)
    //    {
    //        mBundle = request.assetBundle;
    //        downloadPopup.SetActive(false);
    //    }
    //    else
    //    {
    //        Debug.Log(request.error);
    //    }
    //    yield return null; 
    //}
    private IEnumerator GetBundle()
    {
        for(int i = 0; i< mUri.Length; i++)
        {
            using (var www = new UnityWebRequest(mUri[i], UnityWebRequest.kHttpVerbGET))
            {
                www.downloadHandler = new DownloadHandlerAssetBundle(mUri[i], 0, 0);
                www.SendWebRequest();
                while (!www.isDone)
                {
                    progress?.Invoke(www.downloadProgress);                    
                    yield return null;
                }
                if (!www.isNetworkError)
                {
                    mBundle[i] = DownloadHandlerAssetBundle.GetContent(www);
                    
                }
            }
        }
        isAssetReady = true;
        DownloadComplete?.Invoke();
    }
    public Sprite GetSprite(string name)
    {
        Sprite img = null;
        for(int i = 0; i< mBundle.Length; i++)
        {
            img = mBundle[i].LoadAsset<Sprite>(name);
            if (img != null)
            {
                break;
            }
        }
        return img;
    }
}
