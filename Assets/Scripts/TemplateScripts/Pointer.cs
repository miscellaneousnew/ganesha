﻿using UnityEngine;
using UnityEngine.UI;
public class Pointer : MonoBehaviour
{
    [SerializeField]
    Button btn;
    public void SetupPointer(string id)
    {
        name = id;
        btn.onClick.AddListener(delegate { GameManager.instance.LoadItemForCategory(name); gameObject.SetActive(false); });
    }
    private void OnDestroy()
    {
        btn.onClick.RemoveAllListeners();
    }
}
