﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ArtElement : MonoBehaviour
{
    [SerializeField]
    Sprite[] backgroundSprites;
    [SerializeField]
    Image backgroundImg;
    [SerializeField]
    RawImage profilePic;
    [SerializeField]
    TextMeshProUGUI username;
    [SerializeField]
    Button visitButton;

    public void CreateArtElement(int index,string playerId,string playerName,string templateId)
    {
        GameInstanceManager.Instance.GetProfileImage(profilePic, playerId);
        backgroundImg.sprite = backgroundSprites[index % backgroundSprites.Length];
        username.text = playerName;
        name = templateId;
        visitButton.onClick.AddListener(delegate { GameInstanceManager.Instance.DisplayTemplate(transform.name); });
    }
    private void OnDestroy()
    {
        visitButton.onClick.RemoveAllListeners();
    }
}
