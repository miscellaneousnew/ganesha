using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CategoryElement : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI categoryName;
    string myId;
    public void createCategory(string id){
        myId = id;
        categoryName.text = id;
    }
    public void OnClick(){
        GameManager.instance.LoadItemForCategory(myId);
    }
}
