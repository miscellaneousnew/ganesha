﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class CompleteChallengeElement : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI titleTxt;
    [SerializeField]
    TextMeshProUGUI ratingTxt;
    [SerializeField]
    GameObject container;
    [SerializeField]
    GameObject inReviewTxt;
    [SerializeField]
    TextMeshProUGUI gemAmountTxt;
    [SerializeField]
    TextMeshProUGUI coinAmountTxt;

    public void CreateTemplate(string title, float rating, int gemAmount, int coinAmount, bool inReview)
    {
        titleTxt.text = title;
        ratingTxt.text = rating.ToString();
        if(rating == 0)
        {
            gemAmountTxt.text = "0";
            coinAmountTxt.text = "0";
        }
        else
        {
            gemAmountTxt.text = ((int)((gemAmount / 5) * rating)).ToString();
            coinAmountTxt.text = ((int)((coinAmount / 5) * rating)).ToString();
        }
        container.SetActive(!inReview);
        ratingTxt.gameObject.SetActive(!inReview);
        inReviewTxt.SetActive(inReview);
    }
}
