﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class FriendsElement : MonoBehaviour
{
    [SerializeField]
    RawImage profilePic;
    [SerializeField]
    TextMeshProUGUI username;
    [SerializeField]
    Button visitButton;

    public void CreateFriendElement(string userId, string name, string templateId)
    {
        GameInstanceManager.Instance.GetProfileImage(profilePic, userId);
        name = templateId;
        username.text = name;
        visitButton.onClick.AddListener(delegate { GameInstanceManager.Instance.DisplayTemplate(transform.name); });
    }
    private void OnDestroy()
    {
        visitButton.onClick.RemoveAllListeners();
    }
}
