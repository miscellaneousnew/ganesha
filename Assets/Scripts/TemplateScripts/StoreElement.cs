﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class StoreElement : MonoBehaviour
{
    [SerializeField]
    Sprite gemSprite;
    [SerializeField]
    Sprite goldSprite;
    [SerializeField]
    Image itemType;
    [SerializeField]
    TextMeshProUGUI itemQuantiy;
    [SerializeField]
    TextMeshProUGUI itemSalePercent;
    [SerializeField]
    TextMeshProUGUI itemAmount;
    [SerializeField]
    Button itemButton;
    public void CreateStoreElement(GameCurrency gameCurrency,int quantity, int percentage, int amount, string storeItemId)
    {
        name = storeItemId;
        if (gameCurrency == GameCurrency.Gem)
        {
            itemType.sprite = gemSprite;
        }
        else
        {
            itemType.sprite = goldSprite;
        }
        itemQuantiy.text = quantity.ToString();
        itemSalePercent.text = percentage.ToString() + " %";
        itemAmount.text = "RS. " + amount.ToString();
        itemButton.onClick.AddListener(delegate { PurchaseManager.instance.BuyProduct(name); });
    }
    private void OnDestroy()
    {
        itemButton.onClick.RemoveAllListeners();
    }
}
