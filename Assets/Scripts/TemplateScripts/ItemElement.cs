﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ItemElement : MonoBehaviour
{
    public Sprite Gem;
    public Sprite Gold;
    [SerializeField]
    Image currencyType;
    [SerializeField]
    Image item;
    [SerializeField]
    TextMeshProUGUI amount;
    GameCurrency itemCurrency;
    string id;
    public void CreateItem(bool isGem,string itemPath,int amount,string Id){
        if (isGem)
        {
            currencyType.sprite = Gem;
            itemCurrency = GameCurrency.Gem;
        }
        else
        {
            currencyType.sprite = Gold;
            itemCurrency = GameCurrency.Gold;
        }
        item.sprite = LoadingManager.Instance.GetSprite(itemPath);
        id = Id;
        this.amount.text = amount.ToString();
    }
    public void OnClick(){
        GameManager.instance.CreateElement(int.Parse(amount.text),itemCurrency,item.sprite,id);
    }
}
