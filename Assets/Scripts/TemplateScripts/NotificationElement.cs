﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class NotificationElement : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI msgTxt;
    public void SetupMsg(string msg)
    {
        msgTxt.text = msg;
    }
}
