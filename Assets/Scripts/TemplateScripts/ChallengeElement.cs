﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class ChallengeElement : MonoBehaviour
{
    [SerializeField]
    RawImage img;
    [SerializeField]
    TextMeshProUGUI titleTxt;
    [SerializeField]
    TextMeshProUGUI gemAmountTxt;
    [SerializeField]
    TextMeshProUGUI coinAmountTxt;
    [SerializeField]
    Button designBtn;
    string challengeDes;
    Challenges c;
    public async void CreateChallengeElement(Challenges challenges)
    {
        img.texture = await GameInstanceManager.Instance.GetImage(challenges.imageName);
        img.enabled = true;
        titleTxt.text = challenges.title1;
        challengeDes = challenges.description;
        gemAmountTxt.text = challenges.gemAmount.ToString();
        coinAmountTxt.text = challenges.coinAmount.ToString();
        c = challenges;
        designBtn.onClick.AddListener(delegate { MenuManager.instance.LoadChallengePopup(c); });
    }
    private void OnDestroy()
    {
        designBtn.onClick.RemoveAllListeners();
    }
}
