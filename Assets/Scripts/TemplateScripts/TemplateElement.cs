﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class TemplateElement : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI templateName;
    [SerializeField]
    Button editTemplate;
    [SerializeField]
    Button liveTemplate;
    public void SetupTemplate(string templateId, string templateName, bool isLive)
    {
        this.templateName.text = templateName;
        name = templateId;
        liveTemplate.interactable = !isLive;
        editTemplate.onClick.AddListener(delegate { GameInstanceManager.Instance.LoadTemplate(transform.name); });
        liveTemplate.onClick.AddListener(delegate { liveTemplate.interactable = false; GameInstanceManager.Instance.MakeTemplateLive(transform.name); });
    }
    private void OnDestroy()
    {
        editTemplate.onClick.RemoveAllListeners();
        liveTemplate.onClick.RemoveAllListeners();
    }
}
